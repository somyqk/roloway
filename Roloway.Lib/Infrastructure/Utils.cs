﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using Roloway.Lib.Models;

namespace Roloway.Lib.Infrastructure {
    internal static class Utils {
        public static bool BytesAreSame(byte[] a, byte[] b) {
            int len = a.Length;
            if (b.Length < a.Length) {
                return false;
            }

            for (int i = 0; i < len; i++) {
                if (a[i] != b[i]) {
                    return false;
                }
            }
            return true;
        }

        public static long FirstNonZero(byte[] bytes) {
            long i = bytes.LongLength - 1;
            while (i >= 0 && bytes[i] == 0) {
                i--;
            }

            return i;
        }

        public static byte[] ReverseAndFill(byte[] bytes, long length, byte fill) {
            var blength = bytes.LongLength;
            var result = new byte[length];
            for (long i = 0; i < result.LongLength; i++) {
                result[i] = i >= blength ? fill : bytes[blength - i - 1];
            }
            return result;
        }

        public static FieldInfo[] GetFields(Type type) {
            return type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        }

        public static FieldInfo GetField(Type type, string name) {
            return type.GetField(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        }

        public static void WriteBytesReversed(byte[] bytes, Stream stream, Tables tables) {
            var start = FirstNonZero(bytes);
            var size = (byte) (start + 1);

            if (size == 0) {
                size++;
                stream.WriteByte(size);
                stream.WriteByte(0);
            }
            else {
                stream.WriteByte(size);
            }

            for (long i = start; i >= 0; i--) {
                stream.WriteByte(bytes[i]);
            }
        }

        public static byte[] ReadBytesReversed(Stream stream, int typeSize) {
            var byt = new byte[1];
            var read = stream.Read(byt, 0, 1);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }

            var size = byt[0];
            var bytes = new byte[size];
            read = stream.Read(bytes, 0, bytes.Length);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }

            bytes = ReverseAndFill(bytes, typeSize, 0);
            return bytes;
        }

        public static IList GetListOrArrayInstance(Type type, int length) {
            var elementType = ListOrArrayElementType(type);

            if (type.IsArray) {
                return Array.CreateInstance(elementType, length);
            }
            if (type.IsGenericType && typeof (IList).IsAssignableFrom(type)) {
                var ta = type.GetGenericArguments()[0];
                if (ta != null) {
                    return (IList) Activator.CreateInstance(type);
                }
            }

            throw new SerializationException("Unexpected type, array or generic list is expected");
        }

        public static bool IsListOrArray(Type type) {
            return typeof (IList).IsAssignableFrom(type);
        }

        public static bool IsList(Type type) {
            return type.IsGenericType && typeof (IList).IsAssignableFrom(type);
        }

        public static Type ListOrArrayElementType(Type collectionType) {
            if (collectionType.IsArray) {
                return collectionType.GetElementType();
            }
            if (collectionType.IsGenericType && typeof (IList).IsAssignableFrom(collectionType)) {
                var type = collectionType.GetGenericArguments()[0];
                if (type != null) {
                    return type;
                }
            }
            throw new SerializationException("Unexpected type, array or generic list is expected");
        }
    }
}