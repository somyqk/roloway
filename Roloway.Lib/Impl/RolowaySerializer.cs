﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using Roloway.Lib.Abstract;
using Roloway.Lib.Impl.Serializers.Meta;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl {
    public sealed class RolowaySerializer {
        private Stream _stream;
        private LinkedList<ISbSerializer> _serializers;

        internal RolowaySerializer(Stream stream, bool serializeHeader) {
            _stream = stream;
            var serializers = new List<ISbSerializer>();
            if (serializeHeader) {
                serializers.Add(new HeaderSerializer());
            }
            serializers.Add(new FieldTypesSerializer());
            serializers.Add(new FieldNamesSerializer());
            serializers.Add(new FieldValuesSerializer());

            _serializers = new LinkedList<ISbSerializer>(serializers);
        }

        public RolowaySerializer(Stream stream) : this(stream, true) {}

        public void Serialize(object obj) {
            if (obj == null) {
                throw new ArgumentNullException("obj");
            }
            CheckSerializableAttribute(obj.GetType());

            var node = _serializers.First;
            var tables = new Tables();
            while (node != null) {
                node.Value.Serialize(_stream, obj, tables);
                node = node.Next;
            }
        }

        public object Deserialize(Type type) {
            CheckSerializableAttribute(type);

            var instance = FormatterServices.GetUninitializedObject(type);

            var node = _serializers.First;
            var tables = new Tables();
            while (node != null) {
                try {
                    node.Value.Deserialize(_stream, type, tables, instance);
                }
                catch (Exception e) {
                    tables.Types.Clear();
                    tables.Names.Clear();
                    throw;
                }
                node = node.Next;
            }
            return instance;
        }

        private void CheckSerializableAttribute(Type type) {
            var attrs = type.GetCustomAttributes(typeof (SerializableAttribute), false);
            if (attrs.Length == 0) {
                throw new SerializationException("Type is not defined as serializable");
            }
        }
    }
}