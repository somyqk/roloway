﻿using System;
using System.IO;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.ValueType {
    internal sealed class EnumSerializer : AbstractSerializer {
        public EnumSerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            var value = (Int32) obj;
            var bytes = BitConverter.GetBytes(value);
            Utils.WriteBytesReversed(bytes, stream, tables);
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var bytes = Utils.ReadBytesReversed(stream, 4);
            int intResult = BitConverter.ToInt32(bytes, 0);
            object result;
            if (instance != null) {
                result = Enum.ToObject(tables.Types[Index], intResult);
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, result);
                return null;
            }
            result = Enum.ToObject(type, intResult);
            return result;
        }
    }
}