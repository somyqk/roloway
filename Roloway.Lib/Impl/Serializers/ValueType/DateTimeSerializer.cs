﻿using System;
using System.IO;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.ValueType {
    internal sealed class DateTimeSerializer : AbstractSerializer {
        public DateTimeSerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            var value = (DateTime) obj;
            var bytes = BitConverter.GetBytes(value.ToBinary());
            Utils.WriteBytesReversed(bytes, stream, tables);
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var bytes = Utils.ReadBytesReversed(stream, 8);
            DateTime result = DateTime.FromBinary(BitConverter.ToInt64(bytes, 0));
            if (instance != null) {
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, result);
                return null;
            }
            return result;
        }
    }
}