﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.ValueType.Numeric {
    internal sealed class SByteSerializer : AbstractSerializer {
        public SByteSerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            byte value = (byte) (sbyte) obj;
            stream.WriteByte(value);
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var byt = new byte[1];
            var read = stream.Read(byt, 0, 1);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }

            if (instance != null) {
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, (sbyte) byt[0]);
                return null;
            }
            return (sbyte) byt[0];
        }
    }
}