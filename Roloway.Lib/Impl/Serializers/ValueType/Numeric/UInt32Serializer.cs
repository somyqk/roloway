﻿using System;
using System.IO;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.ValueType.Numeric {
    internal sealed class UInt32Serializer : AbstractSerializer {
        public UInt32Serializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            var value = (UInt32) obj;
            var bytes = BitConverter.GetBytes(value);
            Utils.WriteBytesReversed(bytes, stream, tables);
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var bytes = Utils.ReadBytesReversed(stream, 4);
            uint result = BitConverter.ToUInt32(bytes, 0);
            if (instance != null) {
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, result);
                return null;
            }
            return result;
        }
    }
}