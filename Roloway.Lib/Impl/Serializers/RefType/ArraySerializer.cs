﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.RefType {
    internal sealed class ArraySerializer : AbstractSerializer {
        public ArraySerializer(int index) : base(index) {}

        public override void Serialize(Stream stream, object obj, Tables tables) {
            if (obj == null) {
                stream.WriteByte(0);
                return;
            }
            stream.WriteByte(1);

            var value = (IList) obj;
            var count = value.Count;
            if (count == 0) {
                stream.WriteByte(0);
                return;
            }
            stream.WriteByte(1);

            var cbytes = BitConverter.GetBytes(count);
            var elementType = Utils.ListOrArrayElementType(obj.GetType());
            Utils.WriteBytesReversed(cbytes, stream, tables);
            var serializer = Factory.Instance.GetSerializer(elementType, -1);
            foreach (var item in value) {
                serializer.Serialize(stream, item, null);
            }
        }

        public override object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var byt = new byte[1];
            var read = stream.Read(byt, 0, 1);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }
            if (byt[0] == 0) {
                return null;
            }

            read = stream.Read(byt, 0, 1);
            if (read <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }
            IList result;
            if (byt[0] == 0) {
                result = Utils.GetListOrArrayInstance(instance == null ? type : tables.Types[Index], 0);
            }
            else {
                var cbytes = Utils.ReadBytesReversed(stream, 4);
                int count = BitConverter.ToInt32(cbytes, 0);
                result = Utils.GetListOrArrayInstance(instance == null ? type : tables.Types[Index], count);
                var elementType = Utils.ListOrArrayElementType(result.GetType());
                var serializer = Factory.Instance.GetSerializer(elementType, -1);
                for (int i = 0; i < count; i++) {
                    var v = serializer.Deserialize(stream, elementType, null, null);
                    if (Utils.IsList(result.GetType())) {
                        result.Add(v);
                    }
                    else {
                        result[i] = v;
                    }
                }
            }

            if (instance != null) {
                Utils.GetField(type, tables.Names[Index]).SetValue(instance, result);
                return null;
            }
            return result;
        }
    }
}