﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.Meta {
    internal sealed class HeaderSerializer : ISbSerializer {
        private readonly byte[] _bytes = {67, 83, 69, 33};

        public void Serialize(Stream stream, object obj, Tables tables) {
            stream.Write(_bytes, 0, _bytes.Length);

            var nbytes = Encoding.UTF8.GetBytes(obj.GetType().FullName);
            var lbytes = BitConverter.GetBytes(nbytes.Length);
            Utils.WriteBytesReversed(lbytes, stream, tables);
            stream.Write(nbytes, 0, nbytes.Length);
        }

        public object Deserialize(Stream stream, Type type, Tables tables, object result) {
            var bytes = new byte[4];
            var read = stream.Read(bytes, 0, 4);
            if (read == 4) {
                for (int i = 0; i < 4; i++) {
                    if (bytes[i] != _bytes[i]) {
                        throw new SerializationException("Could not recognize binary format");
                    }
                }
            }
            else {
                throw new SerializationException("Could not recognize binary format");
            }

            var lbytes = Utils.ReadBytesReversed(stream, 4);
            var length = BitConverter.ToInt32(lbytes, 0);
            var nbytes = new byte[length];
            stream.Read(nbytes, 0, nbytes.Length);
            var fullName = Encoding.UTF8.GetString(nbytes);

            if (type.FullName != fullName) {
                throw new SerializationException("Inconsistent type. Maybe you are trying to deserialize different type than the serialized one");
            }

            return null;
        }
    }
}