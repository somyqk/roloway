﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using Roloway.Lib.Abstract;
using Roloway.Lib.Infrastructure;
using Roloway.Lib.Models;

namespace Roloway.Lib.Impl.Serializers.Meta {
    internal sealed class FieldTypesSerializer : ISbSerializer {
        private readonly Dictionary<Type, byte> _codes = new Dictionary<Type, byte> {
            {typeof (Boolean), 0xef},
            {typeof (Byte), 0xee},
            {typeof (Char), 0xed},
            {typeof (Decimal), 0xec},
            {typeof (Double), 0xeb},
            {typeof (Single), 0xe0},
            {typeof (Int32), 0xe9},
            {typeof (Int64), 0xe8},
            {typeof (SByte), 0xe7},
            {typeof (Int16), 0xe6},
            {typeof (UInt32), 0xe4},
            {typeof (UInt64), 0xe3},
            {typeof (UInt16), 0xe2},
            {typeof (String), 0xe1},
            {typeof (DateTime), 0xdb}
        };

        public void Serialize(Stream stream, object obj, Tables tables) {
            var fields = Utils.GetFields(obj.GetType());
            for (int i = 0; i < fields.Length; i++) {
                var field = fields[i];
                var fieldType = field.FieldType;
                var code = GetCode(fieldType);

                if (code == 0) {
                    tables.Types.Clear();
                    throw new SerializationException("Type " + fieldType.FullName + " is not supported yet");
                }

                tables.Types.Add(i, fieldType);
                stream.WriteByte(code);

                if (Utils.IsListOrArray(fieldType)) {
                    var elementType = Utils.ListOrArrayElementType(fieldType);
                    var elementCode = GetCode(elementType);
                    stream.WriteByte(elementCode);
                }
            }
            stream.WriteByte(0);
        }

        public object Deserialize(Stream stream, Type type, Tables tables, object instance) {
            var fields = Utils.GetFields(type);
            var byt = new byte[1];

            for (int i = 0; i < fields.Length; i++) {
                var field = fields[i];
                var fieldType = field.FieldType;

                var read = stream.Read(byt, 0, 1);
                if (read <= 0) {
                    throw new SerializationException("Unexpected end of stream");
                }

                if (byt[0] == 0xea) {
                    if (!fieldType.IsEnum) {
                        throw new SerializationException("Type mismatch, enum type expected");
                    }
                    tables.Types.Add(i, fieldType);
                    continue;
                }
                if (byt[0] == 0xda) {
                    if (!Utils.IsListOrArray(fieldType)) {
                        throw new SerializationException("Type mismatch, list or array expected");
                    }

                    var elementType = Utils.ListOrArrayElementType(fieldType);
                    var elementCode = GetCode(elementType);

                    var byt2 = new byte[1];
                    read = stream.Read(byt2, 0, 1);
                    if (read <= 0) {
                        throw new SerializationException("Unexpected end of stream");
                    }
                    if (elementCode != byt2[0]) {
                        throw new SerializationException("Element type of list or array is unmatched");
                    }

                    tables.Types.Add(i, fieldType);
                    continue;
                }
                if (byt[0] == 0xdc) {
                    if (!fieldType.IsClass) {
                        throw new SerializationException("Type mismatch, class expected");
                    }
                    tables.Types.Add(i, fieldType);
                    continue;
                }
                if (byt[0] == 0xe5) {
                    if (!fieldType.IsValueType) {
                        throw new SerializationException("Type mismatch, struct expected");
                    }
                    tables.Types.Add(i, fieldType);
                    continue;
                }

                if (!_codes.ContainsValue(byt[0])) {
                    throw new SerializationException("Unrecognized type code");
                }
                if (!_codes.ContainsKey(fieldType)) {
                    throw new SerializationException("Type " + fieldType.FullName + " is not supported yet");
                }
                if (_codes[fieldType] != byt[0]) {
                    throw new SerializationException("Types not match");
                }

                tables.Types.Add(i, fieldType);
            }

            var last = stream.Read(byt, 0, 1);
            if (last <= 0) {
                throw new SerializationException("Unexpected end of stream");
            }
            if (byt[0] != 0) {
                throw new SerializationException("Unrecognized serialization format");
            }

            return null;
        }

        private byte GetCode(Type fieldType) {
            if (_codes.ContainsKey(fieldType)) {
                return _codes[fieldType];
            }

            if (fieldType.IsEnum) {
                return 0xea;
            }

            if (Utils.IsListOrArray(fieldType)) {
                return 0xda;
            }

            if (fieldType.IsClass) {
                return 0xdc;
            }

            if (fieldType.IsValueType) {
                return 0xe5;
            }

            return 0;
        }
    }
}