﻿using System;
using System.IO;
using Roloway.Lib.Models;

namespace Roloway.Lib.Abstract {
    public abstract class AbstractSerializer : ISbSerializer {
        protected int Index;

        protected AbstractSerializer(int index) {
            Index = index;
        }

        public abstract void Serialize(Stream stream, object obj, Tables tables);

        public abstract object Deserialize(Stream stream, Type type, Tables tables, object instance);
    }
}