﻿using System;
using System.IO;
using Roloway.Lib.Models;

namespace Roloway.Lib.Abstract {
    /// <summary>
    /// ISbSerializer - Simple binary serializer
    /// </summary>
    public interface ISbSerializer {
        void Serialize(Stream stream, object obj, Tables tables);

        object Deserialize(Stream stream, Type type, Tables tables, object result);
    }
}