﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class DateTime_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new DateTimesClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (DateTimesClass) s2.Deserialize(typeof (DateTimesClass));

                Assert.AreEqual(o1.PrFieldDateTime0, o2.PrFieldDateTime0);
                Assert.AreEqual(o1.PrFieldDateTime1, o2.PrFieldDateTime1);
                Assert.AreEqual(o1.PrFieldDateTime2, o2.PrFieldDateTime2);
                Assert.AreEqual(o1.PrFieldDateTime3, o2.PrFieldDateTime3);
                Assert.AreEqual(o1.PrFieldDateTime4, o2.PrFieldDateTime4);
                Assert.AreEqual(o1._pubFieldDateTime0, o2._pubFieldDateTime0);
                Assert.AreEqual(o1._pubFieldDateTime1, o2._pubFieldDateTime1);
                Assert.AreEqual(o1._pubFieldDateTime2, o2._pubFieldDateTime2);
                Assert.AreEqual(o1._pubFieldDateTime3, o2._pubFieldDateTime3);
                Assert.AreEqual(o1._pubFieldDateTime4, o2._pubFieldDateTime4);
                Assert.AreEqual(o1.PropertyDateTime0, o2.PropertyDateTime0);
                Assert.AreEqual(o1.PropertyDateTime1, o2.PropertyDateTime1);
                Assert.AreEqual(o1.PropertyDateTime2, o2.PropertyDateTime2);
                Assert.AreEqual(o1.PropertyDateTime3, o2.PropertyDateTime3);
                Assert.AreEqual(o1.PropertyDateTime4, o2.PropertyDateTime4);
                Assert.AreEqual(o1.PropertyDateTime5, o2.PropertyDateTime5);
                Assert.AreEqual(o1.PropertyDateTime6, o2.PropertyDateTime6);
                Assert.AreEqual(o1.PropertyDateTime7, o2.PropertyDateTime7);
                Assert.AreEqual(o1.PropertyDateTime8, o2.PropertyDateTime8);
                Assert.AreEqual(o1.PropertyDateTime9, o2.PropertyDateTime9);
                Assert.AreEqual(o1.PropertyDateTime10, o2.PropertyDateTime10);
                Assert.AreEqual(o1.PropertyDateTime11, o2.PropertyDateTime11);
            }
        }
    }

    [Serializable]
    internal class DateTimesClass {
        private DateTime _prFieldDateTime0;
        private DateTime _prFieldDateTime1;
        private DateTime _prFieldDateTime2;
        private DateTime _prFieldDateTime3;
        private DateTime _prFieldDateTime4;

        public DateTime PrFieldDateTime0 {
            get { return _prFieldDateTime0; }
        }

        public DateTime PrFieldDateTime1 {
            get { return _prFieldDateTime1; }
        }

        public DateTime PrFieldDateTime2 {
            get { return _prFieldDateTime2; }
        }

        public DateTime PrFieldDateTime3 {
            get { return _prFieldDateTime3; }
        }

        public DateTime PrFieldDateTime4 {
            get { return _prFieldDateTime4; }
        }

        public DateTime _pubFieldDateTime0;
        public DateTime _pubFieldDateTime1;
        public DateTime _pubFieldDateTime2;
        public DateTime _pubFieldDateTime3;
        public DateTime _pubFieldDateTime4;
        public DateTime PropertyDateTime0 { get; set; }
        public DateTime PropertyDateTime1 { get; set; }
        public DateTime PropertyDateTime2 { get; set; }
        public DateTime PropertyDateTime3 { get; set; }
        public DateTime PropertyDateTime4 { get; set; }
        public DateTime PropertyDateTime5 { get; set; }
        public DateTime PropertyDateTime6 { get; set; }
        public DateTime PropertyDateTime7 { get; set; }
        public DateTime PropertyDateTime8 { get; set; }
        public DateTime PropertyDateTime9 { get; set; }
        public DateTime PropertyDateTime10 { get; set; }
        public DateTime PropertyDateTime11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldDateTime0 = Utils.Default<DateTime>();
            _prFieldDateTime1 = Utils.RandomValue<DateTime>(r);
            _prFieldDateTime2 = Utils.Default<DateTime>();
            _prFieldDateTime3 = Utils.RandomValue<DateTime>(r);
            _prFieldDateTime4 = Utils.Default<DateTime>();
            _pubFieldDateTime0 = Utils.Default<DateTime>();
            _pubFieldDateTime1 = Utils.RandomValue<DateTime>(r);
            _pubFieldDateTime2 = Utils.Default<DateTime>();
            _pubFieldDateTime3 = Utils.RandomValue<DateTime>(r);
            _pubFieldDateTime4 = Utils.Default<DateTime>();
            PropertyDateTime0 = Utils.Default<DateTime>();
            PropertyDateTime1 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime2 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime3 = Utils.Default<DateTime>();
            PropertyDateTime4 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime5 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime6 = Utils.Default<DateTime>();
            PropertyDateTime7 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime8 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime9 = Utils.Default<DateTime>();
            PropertyDateTime10 = Utils.RandomValue<DateTime>(r);
            PropertyDateTime11 = Utils.RandomValue<DateTime>(r);
        }
    }
}