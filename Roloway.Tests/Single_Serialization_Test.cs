﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Single_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new SinglesClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (SinglesClass) s2.Deserialize(typeof (SinglesClass));

                Assert.AreEqual(o1.PrFieldSingle0, o2.PrFieldSingle0);
                Assert.AreEqual(o1.PrFieldSingle1, o2.PrFieldSingle1);
                Assert.AreEqual(o1.PrFieldSingle2, o2.PrFieldSingle2);
                Assert.AreEqual(o1.PrFieldSingle3, o2.PrFieldSingle3);
                Assert.AreEqual(o1.PrFieldSingle4, o2.PrFieldSingle4);
                Assert.AreEqual(o1._pubFieldSingle0, o2._pubFieldSingle0);
                Assert.AreEqual(o1._pubFieldSingle1, o2._pubFieldSingle1);
                Assert.AreEqual(o1._pubFieldSingle2, o2._pubFieldSingle2);
                Assert.AreEqual(o1._pubFieldSingle3, o2._pubFieldSingle3);
                Assert.AreEqual(o1._pubFieldSingle4, o2._pubFieldSingle4);
                Assert.AreEqual(o1.PropertySingle0, o2.PropertySingle0);
                Assert.AreEqual(o1.PropertySingle1, o2.PropertySingle1);
                Assert.AreEqual(o1.PropertySingle2, o2.PropertySingle2);
                Assert.AreEqual(o1.PropertySingle3, o2.PropertySingle3);
                Assert.AreEqual(o1.PropertySingle4, o2.PropertySingle4);
                Assert.AreEqual(o1.PropertySingle5, o2.PropertySingle5);
                Assert.AreEqual(o1.PropertySingle6, o2.PropertySingle6);
                Assert.AreEqual(o1.PropertySingle7, o2.PropertySingle7);
                Assert.AreEqual(o1.PropertySingle8, o2.PropertySingle8);
                Assert.AreEqual(o1.PropertySingle9, o2.PropertySingle9);
                Assert.AreEqual(o1.PropertySingle10, o2.PropertySingle10);
                Assert.AreEqual(o1.PropertySingle11, o2.PropertySingle11);
            }
        }
    }

    [Serializable]
    internal class SinglesClass {
        private Single _prFieldSingle0;
        private Single _prFieldSingle1;
        private Single _prFieldSingle2;
        private Single _prFieldSingle3;
        private Single _prFieldSingle4;

        public Single PrFieldSingle0 {
            get { return _prFieldSingle0; }
        }

        public Single PrFieldSingle1 {
            get { return _prFieldSingle1; }
        }

        public Single PrFieldSingle2 {
            get { return _prFieldSingle2; }
        }

        public Single PrFieldSingle3 {
            get { return _prFieldSingle3; }
        }

        public Single PrFieldSingle4 {
            get { return _prFieldSingle4; }
        }

        public Single _pubFieldSingle0;
        public Single _pubFieldSingle1;
        public Single _pubFieldSingle2;
        public Single _pubFieldSingle3;
        public Single _pubFieldSingle4;
        public Single PropertySingle0 { get; set; }
        public Single PropertySingle1 { get; set; }
        public Single PropertySingle2 { get; set; }
        public Single PropertySingle3 { get; set; }
        public Single PropertySingle4 { get; set; }
        public Single PropertySingle5 { get; set; }
        public Single PropertySingle6 { get; set; }
        public Single PropertySingle7 { get; set; }
        public Single PropertySingle8 { get; set; }
        public Single PropertySingle9 { get; set; }
        public Single PropertySingle10 { get; set; }
        public Single PropertySingle11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldSingle0 = Utils.Default<Single>();
            _prFieldSingle1 = Utils.RandomValue<Single>(r);
            _prFieldSingle2 = Utils.Default<Single>();
            _prFieldSingle3 = Utils.RandomValue<Single>(r);
            _prFieldSingle4 = Utils.Default<Single>();
            _pubFieldSingle0 = Utils.Default<Single>();
            _pubFieldSingle1 = Utils.RandomValue<Single>(r);
            _pubFieldSingle2 = Utils.Default<Single>();
            _pubFieldSingle3 = Utils.RandomValue<Single>(r);
            _pubFieldSingle4 = Utils.Default<Single>();
            PropertySingle0 = Utils.Default<Single>();
            PropertySingle1 = Utils.RandomValue<Single>(r);
            PropertySingle2 = Utils.RandomValue<Single>(r);
            PropertySingle3 = Utils.Default<Single>();
            PropertySingle4 = Utils.RandomValue<Single>(r);
            PropertySingle5 = Utils.RandomValue<Single>(r);
            PropertySingle6 = Utils.Default<Single>();
            PropertySingle7 = Utils.RandomValue<Single>(r);
            PropertySingle8 = Utils.RandomValue<Single>(r);
            PropertySingle9 = Utils.Default<Single>();
            PropertySingle10 = Utils.RandomValue<Single>(r);
            PropertySingle11 = Utils.RandomValue<Single>(r);
        }
    }
}