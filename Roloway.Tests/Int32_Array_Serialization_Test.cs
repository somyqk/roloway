﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Int32_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new Int32ArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (Int32ArrayClass) s2.Deserialize(typeof (Int32ArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldInt32Arr0, o2.PrFieldInt32Arr0);
                CollectionAssert.AreEqual(o1.PrFieldInt32Arr1, o2.PrFieldInt32Arr1);
                CollectionAssert.AreEqual(o1.PrFieldInt32Arr2, o2.PrFieldInt32Arr2);
                CollectionAssert.AreEqual(o1.PrFieldInt32Arr3, o2.PrFieldInt32Arr3);
                CollectionAssert.AreEqual(o1.PrFieldInt32Arr4, o2.PrFieldInt32Arr4);
                CollectionAssert.AreEqual(o1._pubFieldInt32Arr0, o2._pubFieldInt32Arr0);
                CollectionAssert.AreEqual(o1._pubFieldInt32Arr1, o2._pubFieldInt32Arr1);
                CollectionAssert.AreEqual(o1._pubFieldInt32Arr2, o2._pubFieldInt32Arr2);
                CollectionAssert.AreEqual(o1._pubFieldInt32Arr3, o2._pubFieldInt32Arr3);
                CollectionAssert.AreEqual(o1._pubFieldInt32Arr4, o2._pubFieldInt32Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr0, o2.PropertyInt32Arr0);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr1, o2.PropertyInt32Arr1);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr2, o2.PropertyInt32Arr2);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr3, o2.PropertyInt32Arr3);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr4, o2.PropertyInt32Arr4);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr5, o2.PropertyInt32Arr5);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr6, o2.PropertyInt32Arr6);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr7, o2.PropertyInt32Arr7);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr8, o2.PropertyInt32Arr8);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr9, o2.PropertyInt32Arr9);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr10, o2.PropertyInt32Arr10);
                CollectionAssert.AreEqual(o1.PropertyInt32Arr11, o2.PropertyInt32Arr11);
            }
        }
    }

    [Serializable]
    internal class Int32ArrayClass {
        private Int32[] _prFieldInt32Arr0;
        private Int32[] _prFieldInt32Arr1;
        private Int32[] _prFieldInt32Arr2;
        private Int32[] _prFieldInt32Arr3;
        private Int32[] _prFieldInt32Arr4;

        public Int32[] PrFieldInt32Arr0 {
            get { return _prFieldInt32Arr0; }
        }

        public Int32[] PrFieldInt32Arr1 {
            get { return _prFieldInt32Arr1; }
        }

        public Int32[] PrFieldInt32Arr2 {
            get { return _prFieldInt32Arr2; }
        }

        public Int32[] PrFieldInt32Arr3 {
            get { return _prFieldInt32Arr3; }
        }

        public Int32[] PrFieldInt32Arr4 {
            get { return _prFieldInt32Arr4; }
        }

        public Int32[] _pubFieldInt32Arr0;
        public Int32[] _pubFieldInt32Arr1;
        public Int32[] _pubFieldInt32Arr2;
        public Int32[] _pubFieldInt32Arr3;
        public Int32[] _pubFieldInt32Arr4;
        public Int32[] PropertyInt32Arr0 { get; set; }
        public Int32[] PropertyInt32Arr1 { get; set; }
        public Int32[] PropertyInt32Arr2 { get; set; }
        public Int32[] PropertyInt32Arr3 { get; set; }
        public Int32[] PropertyInt32Arr4 { get; set; }
        public Int32[] PropertyInt32Arr5 { get; set; }
        public Int32[] PropertyInt32Arr6 { get; set; }
        public Int32[] PropertyInt32Arr7 { get; set; }
        public Int32[] PropertyInt32Arr8 { get; set; }
        public Int32[] PropertyInt32Arr9 { get; set; }
        public Int32[] PropertyInt32Arr10 { get; set; }
        public Int32[] PropertyInt32Arr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldInt32Arr0 = Utils.Default<Int32[]>();
            _prFieldInt32Arr1 = Utils.RandomArray<Int32>(r);
            _prFieldInt32Arr2 = Utils.Default<Int32[]>();
            _prFieldInt32Arr3 = Utils.EmptyArray<Int32>();
            _prFieldInt32Arr4 = Utils.Default<Int32[]>();
            _pubFieldInt32Arr0 = Utils.Default<Int32[]>();
            _pubFieldInt32Arr1 = Utils.RandomArray<Int32>(r);
            _pubFieldInt32Arr2 = Utils.Default<Int32[]>();
            _pubFieldInt32Arr3 = Utils.EmptyArray<Int32>();
            _pubFieldInt32Arr4 = Utils.Default<Int32[]>();
            PropertyInt32Arr0 = Utils.Default<Int32[]>();
            PropertyInt32Arr1 = Utils.RandomArray<Int32>(r);
            PropertyInt32Arr2 = Utils.Default<Int32[]>();
            PropertyInt32Arr3 = Utils.EmptyArray<Int32>();
            PropertyInt32Arr4 = Utils.Default<Int32[]>();
            PropertyInt32Arr5 = Utils.RandomArray<Int32>(r);
            PropertyInt32Arr6 = Utils.Default<Int32[]>();
            PropertyInt32Arr7 = Utils.RandomArray<Int32>(r);
            PropertyInt32Arr8 = Utils.Default<Int32[]>();
            PropertyInt32Arr9 = Utils.EmptyArray<Int32>();
            PropertyInt32Arr10 = Utils.Default<Int32[]>();
            PropertyInt32Arr11 = Utils.RandomArray<Int32>(r);
        }
    }
}