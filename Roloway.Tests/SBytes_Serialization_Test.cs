﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class SBytes_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new SBytesClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (SBytesClass) s2.Deserialize(typeof (SBytesClass));

                Assert.AreEqual(o1.PrFieldSByte0, o2.PrFieldSByte0);
                Assert.AreEqual(o1.PrFieldSByte1, o2.PrFieldSByte1);
                Assert.AreEqual(o1.PrFieldSByte2, o2.PrFieldSByte2);
                Assert.AreEqual(o1.PrFieldSByte3, o2.PrFieldSByte3);
                Assert.AreEqual(o1.PrFieldSByte4, o2.PrFieldSByte4);
                Assert.AreEqual(o1._pubFieldSByte0, o2._pubFieldSByte0);
                Assert.AreEqual(o1._pubFieldSByte1, o2._pubFieldSByte1);
                Assert.AreEqual(o1._pubFieldSByte2, o2._pubFieldSByte2);
                Assert.AreEqual(o1._pubFieldSByte3, o2._pubFieldSByte3);
                Assert.AreEqual(o1._pubFieldSByte4, o2._pubFieldSByte4);
                Assert.AreEqual(o1.PropertySByte0, o2.PropertySByte0);
                Assert.AreEqual(o1.PropertySByte1, o2.PropertySByte1);
                Assert.AreEqual(o1.PropertySByte2, o2.PropertySByte2);
                Assert.AreEqual(o1.PropertySByte3, o2.PropertySByte3);
                Assert.AreEqual(o1.PropertySByte4, o2.PropertySByte4);
                Assert.AreEqual(o1.PropertySByte5, o2.PropertySByte5);
                Assert.AreEqual(o1.PropertySByte6, o2.PropertySByte6);
                Assert.AreEqual(o1.PropertySByte7, o2.PropertySByte7);
                Assert.AreEqual(o1.PropertySByte8, o2.PropertySByte8);
                Assert.AreEqual(o1.PropertySByte9, o2.PropertySByte9);
                Assert.AreEqual(o1.PropertySByte10, o2.PropertySByte10);
                Assert.AreEqual(o1.PropertySByte11, o2.PropertySByte11);
            }
        }
    }

    [Serializable]
    internal class SBytesClass {
        private SByte _prFieldSByte0;
        private SByte _prFieldSByte1;
        private SByte _prFieldSByte2;
        private SByte _prFieldSByte3;
        private SByte _prFieldSByte4;

        public SByte PrFieldSByte0 {
            get { return _prFieldSByte0; }
        }

        public SByte PrFieldSByte1 {
            get { return _prFieldSByte1; }
        }

        public SByte PrFieldSByte2 {
            get { return _prFieldSByte2; }
        }

        public SByte PrFieldSByte3 {
            get { return _prFieldSByte3; }
        }

        public SByte PrFieldSByte4 {
            get { return _prFieldSByte4; }
        }

        public SByte _pubFieldSByte0;
        public SByte _pubFieldSByte1;
        public SByte _pubFieldSByte2;
        public SByte _pubFieldSByte3;
        public SByte _pubFieldSByte4;
        public SByte PropertySByte0 { get; set; }
        public SByte PropertySByte1 { get; set; }
        public SByte PropertySByte2 { get; set; }
        public SByte PropertySByte3 { get; set; }
        public SByte PropertySByte4 { get; set; }
        public SByte PropertySByte5 { get; set; }
        public SByte PropertySByte6 { get; set; }
        public SByte PropertySByte7 { get; set; }
        public SByte PropertySByte8 { get; set; }
        public SByte PropertySByte9 { get; set; }
        public SByte PropertySByte10 { get; set; }
        public SByte PropertySByte11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldSByte0 = Utils.Default<SByte>();
            _prFieldSByte1 = Utils.RandomValue<SByte>(r);
            _prFieldSByte2 = Utils.Default<SByte>();
            _prFieldSByte3 = Utils.RandomValue<SByte>(r);
            _prFieldSByte4 = Utils.Default<SByte>();
            _pubFieldSByte0 = Utils.Default<SByte>();
            _pubFieldSByte1 = Utils.RandomValue<SByte>(r);
            _pubFieldSByte2 = Utils.Default<SByte>();
            _pubFieldSByte3 = Utils.RandomValue<SByte>(r);
            _pubFieldSByte4 = Utils.Default<SByte>();
            PropertySByte0 = Utils.Default<SByte>();
            PropertySByte1 = Utils.RandomValue<SByte>(r);
            PropertySByte2 = Utils.RandomValue<SByte>(r);
            PropertySByte3 = Utils.Default<SByte>();
            PropertySByte4 = Utils.RandomValue<SByte>(r);
            PropertySByte5 = Utils.RandomValue<SByte>(r);
            PropertySByte6 = Utils.Default<SByte>();
            PropertySByte7 = Utils.RandomValue<SByte>(r);
            PropertySByte8 = Utils.RandomValue<SByte>(r);
            PropertySByte9 = Utils.Default<SByte>();
            PropertySByte10 = Utils.RandomValue<SByte>(r);
            PropertySByte11 = Utils.RandomValue<SByte>(r);
        }
    }
}