﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Byte_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ByteArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ByteArrayClass) s2.Deserialize(typeof (ByteArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldByteArr0, o2.PrFieldByteArr0);
                CollectionAssert.AreEqual(o1.PrFieldByteArr1, o2.PrFieldByteArr1);
                CollectionAssert.AreEqual(o1.PrFieldByteArr2, o2.PrFieldByteArr2);
                CollectionAssert.AreEqual(o1.PrFieldByteArr3, o2.PrFieldByteArr3);
                CollectionAssert.AreEqual(o1.PrFieldByteArr4, o2.PrFieldByteArr4);
                CollectionAssert.AreEqual(o1._pubFieldByteArr0, o2._pubFieldByteArr0);
                CollectionAssert.AreEqual(o1._pubFieldByteArr1, o2._pubFieldByteArr1);
                CollectionAssert.AreEqual(o1._pubFieldByteArr2, o2._pubFieldByteArr2);
                CollectionAssert.AreEqual(o1._pubFieldByteArr3, o2._pubFieldByteArr3);
                CollectionAssert.AreEqual(o1._pubFieldByteArr4, o2._pubFieldByteArr4);
                CollectionAssert.AreEqual(o1.PropertyByteArr0, o2.PropertyByteArr0);
                CollectionAssert.AreEqual(o1.PropertyByteArr1, o2.PropertyByteArr1);
                CollectionAssert.AreEqual(o1.PropertyByteArr2, o2.PropertyByteArr2);
                CollectionAssert.AreEqual(o1.PropertyByteArr3, o2.PropertyByteArr3);
                CollectionAssert.AreEqual(o1.PropertyByteArr4, o2.PropertyByteArr4);
                CollectionAssert.AreEqual(o1.PropertyByteArr5, o2.PropertyByteArr5);
                CollectionAssert.AreEqual(o1.PropertyByteArr6, o2.PropertyByteArr6);
                CollectionAssert.AreEqual(o1.PropertyByteArr7, o2.PropertyByteArr7);
                CollectionAssert.AreEqual(o1.PropertyByteArr8, o2.PropertyByteArr8);
                CollectionAssert.AreEqual(o1.PropertyByteArr9, o2.PropertyByteArr9);
                CollectionAssert.AreEqual(o1.PropertyByteArr10, o2.PropertyByteArr10);
                CollectionAssert.AreEqual(o1.PropertyByteArr11, o2.PropertyByteArr11);
            }
        }
    }

    [Serializable]
    internal class ByteArrayClass {
        private Byte[] _prFieldByteArr0;
        private Byte[] _prFieldByteArr1;
        private Byte[] _prFieldByteArr2;
        private Byte[] _prFieldByteArr3;
        private Byte[] _prFieldByteArr4;

        public Byte[] PrFieldByteArr0 {
            get { return _prFieldByteArr0; }
        }

        public Byte[] PrFieldByteArr1 {
            get { return _prFieldByteArr1; }
        }

        public Byte[] PrFieldByteArr2 {
            get { return _prFieldByteArr2; }
        }

        public Byte[] PrFieldByteArr3 {
            get { return _prFieldByteArr3; }
        }

        public Byte[] PrFieldByteArr4 {
            get { return _prFieldByteArr4; }
        }

        public Byte[] _pubFieldByteArr0;
        public Byte[] _pubFieldByteArr1;
        public Byte[] _pubFieldByteArr2;
        public Byte[] _pubFieldByteArr3;
        public Byte[] _pubFieldByteArr4;
        public Byte[] PropertyByteArr0 { get; set; }
        public Byte[] PropertyByteArr1 { get; set; }
        public Byte[] PropertyByteArr2 { get; set; }
        public Byte[] PropertyByteArr3 { get; set; }
        public Byte[] PropertyByteArr4 { get; set; }
        public Byte[] PropertyByteArr5 { get; set; }
        public Byte[] PropertyByteArr6 { get; set; }
        public Byte[] PropertyByteArr7 { get; set; }
        public Byte[] PropertyByteArr8 { get; set; }
        public Byte[] PropertyByteArr9 { get; set; }
        public Byte[] PropertyByteArr10 { get; set; }
        public Byte[] PropertyByteArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldByteArr0 = Utils.Default<Byte[]>();
            _prFieldByteArr1 = Utils.RandomArray<Byte>(r);
            _prFieldByteArr2 = Utils.Default<Byte[]>();
            _prFieldByteArr3 = Utils.EmptyArray<Byte>();
            _prFieldByteArr4 = Utils.Default<Byte[]>();
            _pubFieldByteArr0 = Utils.Default<Byte[]>();
            _pubFieldByteArr1 = Utils.RandomArray<Byte>(r);
            _pubFieldByteArr2 = Utils.Default<Byte[]>();
            _pubFieldByteArr3 = Utils.EmptyArray<Byte>();
            _pubFieldByteArr4 = Utils.Default<Byte[]>();
            PropertyByteArr0 = Utils.Default<Byte[]>();
            PropertyByteArr1 = Utils.RandomArray<Byte>(r);
            PropertyByteArr2 = Utils.Default<Byte[]>();
            PropertyByteArr3 = Utils.EmptyArray<Byte>();
            PropertyByteArr4 = Utils.Default<Byte[]>();
            PropertyByteArr5 = Utils.RandomArray<Byte>(r);
            PropertyByteArr6 = Utils.Default<Byte[]>();
            PropertyByteArr7 = Utils.RandomArray<Byte>(r);
            PropertyByteArr8 = Utils.Default<Byte[]>();
            PropertyByteArr9 = Utils.EmptyArray<Byte>();
            PropertyByteArr10 = Utils.Default<Byte[]>();
            PropertyByteArr11 = Utils.RandomArray<Byte>(r);
        }
    }
}