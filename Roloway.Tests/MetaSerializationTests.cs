﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class MetaSerializationTests {
        [TestMethod]
        public void Classes_Without_Serializable_Attribute_Should_Fail() {
            using (var stream = new MemoryStream()) {
                //On serialization
                var s1 = new RolowaySerializer(stream);
                try {
                    s1.Serialize(new Class1());
                    Assert.Fail();
                }
                catch (SerializationException e) {
                    Assert.AreEqual(e.Message, "Type is not defined as serializable");
                }

                //On deserialization
                var s2 = new RolowaySerializer(stream);
                try {
                    s2.Deserialize(typeof (Class1));
                    Assert.Fail();
                }
                catch (SerializationException e) {
                    Assert.AreEqual(e.Message, "Type is not defined as serializable");
                }
            }
        }

        [TestMethod]
        public void If_Header_Is_Icorrect_Then_Fail() {
            using (var stream = new MemoryStream()) {
                var b = new byte[] {1, 2, 3};
                stream.Write(b, 0, b.Length);
                stream.Position = 0;

                var s2 = new RolowaySerializer(stream);
                try {
                    var o = s2.Deserialize(typeof (Class2));
                    Assert.Fail();
                }
                catch (SerializationException e) {
                    Assert.AreEqual(e.Message, "Could not recognize binary format");
                }
            }

            using (var stream = new MemoryStream()) {
                var b = new byte[] {1, 2, 3, 5, 6};
                stream.Write(b, 0, b.Length);
                stream.Position = 0;

                var s2 = new RolowaySerializer(stream);
                try {
                    var o = s2.Deserialize(typeof (Class2));
                    Assert.Fail();
                }
                catch (SerializationException e) {
                    Assert.AreEqual(e.Message, "Could not recognize binary format");
                }
            }
        }

        [TestMethod]
        public void If_Deserializing_Different_Type_Than_Serialized_Then_Fail() {
            using (var stream = new MemoryStream()) {
                var s1 = new RolowaySerializer(stream);
                s1.Serialize(new Class2());
                stream.Position = 0;

                var s2 = new RolowaySerializer(stream);
                try {
                    s2.Deserialize(typeof (Class3));
                }
                catch (SerializationException e) {
                    Assert.AreEqual(e.Message, "Inconsistent type. Maybe you are trying to deserialize different type than the serialized one");
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentNullException))]
        public void Passing_Null_Argument_Should_Fail() {
            using (var stream = new MemoryStream()) {
                var s1 = new RolowaySerializer(stream);
                s1.Serialize(null);
            }
        }
    }
}