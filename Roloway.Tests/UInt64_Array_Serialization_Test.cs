﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class UInt64_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new UInt64ArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (UInt64ArrayClass) s2.Deserialize(typeof (UInt64ArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldUInt64Arr0, o2.PrFieldUInt64Arr0);
                CollectionAssert.AreEqual(o1.PrFieldUInt64Arr1, o2.PrFieldUInt64Arr1);
                CollectionAssert.AreEqual(o1.PrFieldUInt64Arr2, o2.PrFieldUInt64Arr2);
                CollectionAssert.AreEqual(o1.PrFieldUInt64Arr3, o2.PrFieldUInt64Arr3);
                CollectionAssert.AreEqual(o1.PrFieldUInt64Arr4, o2.PrFieldUInt64Arr4);
                CollectionAssert.AreEqual(o1._pubFieldUInt64Arr0, o2._pubFieldUInt64Arr0);
                CollectionAssert.AreEqual(o1._pubFieldUInt64Arr1, o2._pubFieldUInt64Arr1);
                CollectionAssert.AreEqual(o1._pubFieldUInt64Arr2, o2._pubFieldUInt64Arr2);
                CollectionAssert.AreEqual(o1._pubFieldUInt64Arr3, o2._pubFieldUInt64Arr3);
                CollectionAssert.AreEqual(o1._pubFieldUInt64Arr4, o2._pubFieldUInt64Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr0, o2.PropertyUInt64Arr0);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr1, o2.PropertyUInt64Arr1);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr2, o2.PropertyUInt64Arr2);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr3, o2.PropertyUInt64Arr3);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr4, o2.PropertyUInt64Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr5, o2.PropertyUInt64Arr5);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr6, o2.PropertyUInt64Arr6);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr7, o2.PropertyUInt64Arr7);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr8, o2.PropertyUInt64Arr8);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr9, o2.PropertyUInt64Arr9);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr10, o2.PropertyUInt64Arr10);
                CollectionAssert.AreEqual(o1.PropertyUInt64Arr11, o2.PropertyUInt64Arr11);
            }
        }
    }

    [Serializable]
    internal class UInt64ArrayClass {
        private UInt64[] _prFieldUInt64Arr0;
        private UInt64[] _prFieldUInt64Arr1;
        private UInt64[] _prFieldUInt64Arr2;
        private UInt64[] _prFieldUInt64Arr3;
        private UInt64[] _prFieldUInt64Arr4;

        public UInt64[] PrFieldUInt64Arr0 {
            get { return _prFieldUInt64Arr0; }
        }

        public UInt64[] PrFieldUInt64Arr1 {
            get { return _prFieldUInt64Arr1; }
        }

        public UInt64[] PrFieldUInt64Arr2 {
            get { return _prFieldUInt64Arr2; }
        }

        public UInt64[] PrFieldUInt64Arr3 {
            get { return _prFieldUInt64Arr3; }
        }

        public UInt64[] PrFieldUInt64Arr4 {
            get { return _prFieldUInt64Arr4; }
        }

        public UInt64[] _pubFieldUInt64Arr0;
        public UInt64[] _pubFieldUInt64Arr1;
        public UInt64[] _pubFieldUInt64Arr2;
        public UInt64[] _pubFieldUInt64Arr3;
        public UInt64[] _pubFieldUInt64Arr4;
        public UInt64[] PropertyUInt64Arr0 { get; set; }
        public UInt64[] PropertyUInt64Arr1 { get; set; }
        public UInt64[] PropertyUInt64Arr2 { get; set; }
        public UInt64[] PropertyUInt64Arr3 { get; set; }
        public UInt64[] PropertyUInt64Arr4 { get; set; }
        public UInt64[] PropertyUInt64Arr5 { get; set; }
        public UInt64[] PropertyUInt64Arr6 { get; set; }
        public UInt64[] PropertyUInt64Arr7 { get; set; }
        public UInt64[] PropertyUInt64Arr8 { get; set; }
        public UInt64[] PropertyUInt64Arr9 { get; set; }
        public UInt64[] PropertyUInt64Arr10 { get; set; }
        public UInt64[] PropertyUInt64Arr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldUInt64Arr0 = Utils.Default<UInt64[]>();
            _prFieldUInt64Arr1 = Utils.RandomArray<UInt64>(r);
            _prFieldUInt64Arr2 = Utils.Default<UInt64[]>();
            _prFieldUInt64Arr3 = Utils.EmptyArray<UInt64>();
            _prFieldUInt64Arr4 = Utils.Default<UInt64[]>();
            _pubFieldUInt64Arr0 = Utils.Default<UInt64[]>();
            _pubFieldUInt64Arr1 = Utils.RandomArray<UInt64>(r);
            _pubFieldUInt64Arr2 = Utils.Default<UInt64[]>();
            _pubFieldUInt64Arr3 = Utils.EmptyArray<UInt64>();
            _pubFieldUInt64Arr4 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr0 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr1 = Utils.RandomArray<UInt64>(r);
            PropertyUInt64Arr2 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr3 = Utils.EmptyArray<UInt64>();
            PropertyUInt64Arr4 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr5 = Utils.RandomArray<UInt64>(r);
            PropertyUInt64Arr6 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr7 = Utils.RandomArray<UInt64>(r);
            PropertyUInt64Arr8 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr9 = Utils.EmptyArray<UInt64>();
            PropertyUInt64Arr10 = Utils.Default<UInt64[]>();
            PropertyUInt64Arr11 = Utils.RandomArray<UInt64>(r);
        }
    }
}