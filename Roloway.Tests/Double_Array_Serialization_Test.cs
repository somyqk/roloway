﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Double_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new DoubleArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (DoubleArrayClass) s2.Deserialize(typeof (DoubleArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldDoubleArr0, o2.PrFieldDoubleArr0);
                CollectionAssert.AreEqual(o1.PrFieldDoubleArr1, o2.PrFieldDoubleArr1);
                CollectionAssert.AreEqual(o1.PrFieldDoubleArr2, o2.PrFieldDoubleArr2);
                CollectionAssert.AreEqual(o1.PrFieldDoubleArr3, o2.PrFieldDoubleArr3);
                CollectionAssert.AreEqual(o1.PrFieldDoubleArr4, o2.PrFieldDoubleArr4);
                CollectionAssert.AreEqual(o1._pubFieldDoubleArr0, o2._pubFieldDoubleArr0);
                CollectionAssert.AreEqual(o1._pubFieldDoubleArr1, o2._pubFieldDoubleArr1);
                CollectionAssert.AreEqual(o1._pubFieldDoubleArr2, o2._pubFieldDoubleArr2);
                CollectionAssert.AreEqual(o1._pubFieldDoubleArr3, o2._pubFieldDoubleArr3);
                CollectionAssert.AreEqual(o1._pubFieldDoubleArr4, o2._pubFieldDoubleArr4);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr0, o2.PropertyDoubleArr0);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr1, o2.PropertyDoubleArr1);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr2, o2.PropertyDoubleArr2);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr3, o2.PropertyDoubleArr3);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr4, o2.PropertyDoubleArr4);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr5, o2.PropertyDoubleArr5);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr6, o2.PropertyDoubleArr6);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr7, o2.PropertyDoubleArr7);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr8, o2.PropertyDoubleArr8);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr9, o2.PropertyDoubleArr9);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr10, o2.PropertyDoubleArr10);
                CollectionAssert.AreEqual(o1.PropertyDoubleArr11, o2.PropertyDoubleArr11);
            }
        }
    }

    [Serializable]
    internal class DoubleArrayClass {
        private Double[] _prFieldDoubleArr0;
        private Double[] _prFieldDoubleArr1;
        private Double[] _prFieldDoubleArr2;
        private Double[] _prFieldDoubleArr3;
        private Double[] _prFieldDoubleArr4;

        public Double[] PrFieldDoubleArr0 {
            get { return _prFieldDoubleArr0; }
        }

        public Double[] PrFieldDoubleArr1 {
            get { return _prFieldDoubleArr1; }
        }

        public Double[] PrFieldDoubleArr2 {
            get { return _prFieldDoubleArr2; }
        }

        public Double[] PrFieldDoubleArr3 {
            get { return _prFieldDoubleArr3; }
        }

        public Double[] PrFieldDoubleArr4 {
            get { return _prFieldDoubleArr4; }
        }

        public Double[] _pubFieldDoubleArr0;
        public Double[] _pubFieldDoubleArr1;
        public Double[] _pubFieldDoubleArr2;
        public Double[] _pubFieldDoubleArr3;
        public Double[] _pubFieldDoubleArr4;
        public Double[] PropertyDoubleArr0 { get; set; }
        public Double[] PropertyDoubleArr1 { get; set; }
        public Double[] PropertyDoubleArr2 { get; set; }
        public Double[] PropertyDoubleArr3 { get; set; }
        public Double[] PropertyDoubleArr4 { get; set; }
        public Double[] PropertyDoubleArr5 { get; set; }
        public Double[] PropertyDoubleArr6 { get; set; }
        public Double[] PropertyDoubleArr7 { get; set; }
        public Double[] PropertyDoubleArr8 { get; set; }
        public Double[] PropertyDoubleArr9 { get; set; }
        public Double[] PropertyDoubleArr10 { get; set; }
        public Double[] PropertyDoubleArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldDoubleArr0 = Utils.Default<Double[]>();
            _prFieldDoubleArr1 = Utils.RandomArray<Double>(r);
            _prFieldDoubleArr2 = Utils.Default<Double[]>();
            _prFieldDoubleArr3 = Utils.EmptyArray<Double>();
            _prFieldDoubleArr4 = Utils.Default<Double[]>();
            _pubFieldDoubleArr0 = Utils.Default<Double[]>();
            _pubFieldDoubleArr1 = Utils.RandomArray<Double>(r);
            _pubFieldDoubleArr2 = Utils.Default<Double[]>();
            _pubFieldDoubleArr3 = Utils.EmptyArray<Double>();
            _pubFieldDoubleArr4 = Utils.Default<Double[]>();
            PropertyDoubleArr0 = Utils.Default<Double[]>();
            PropertyDoubleArr1 = Utils.RandomArray<Double>(r);
            PropertyDoubleArr2 = Utils.Default<Double[]>();
            PropertyDoubleArr3 = Utils.EmptyArray<Double>();
            PropertyDoubleArr4 = Utils.Default<Double[]>();
            PropertyDoubleArr5 = Utils.RandomArray<Double>(r);
            PropertyDoubleArr6 = Utils.Default<Double[]>();
            PropertyDoubleArr7 = Utils.RandomArray<Double>(r);
            PropertyDoubleArr8 = Utils.Default<Double[]>();
            PropertyDoubleArr9 = Utils.EmptyArray<Double>();
            PropertyDoubleArr10 = Utils.Default<Double[]>();
            PropertyDoubleArr11 = Utils.RandomArray<Double>(r);
        }
    }
}