﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Int16_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new Int16sClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (Int16sClass) s2.Deserialize(typeof (Int16sClass));

                Assert.AreEqual(o1.PrFieldInt160, o2.PrFieldInt160);
                Assert.AreEqual(o1.PrFieldInt161, o2.PrFieldInt161);
                Assert.AreEqual(o1.PrFieldInt162, o2.PrFieldInt162);
                Assert.AreEqual(o1.PrFieldInt163, o2.PrFieldInt163);
                Assert.AreEqual(o1.PrFieldInt164, o2.PrFieldInt164);
                Assert.AreEqual(o1._pubFieldInt160, o2._pubFieldInt160);
                Assert.AreEqual(o1._pubFieldInt161, o2._pubFieldInt161);
                Assert.AreEqual(o1._pubFieldInt162, o2._pubFieldInt162);
                Assert.AreEqual(o1._pubFieldInt163, o2._pubFieldInt163);
                Assert.AreEqual(o1._pubFieldInt164, o2._pubFieldInt164);
                Assert.AreEqual(o1.PropertyInt160, o2.PropertyInt160);
                Assert.AreEqual(o1.PropertyInt161, o2.PropertyInt161);
                Assert.AreEqual(o1.PropertyInt162, o2.PropertyInt162);
                Assert.AreEqual(o1.PropertyInt163, o2.PropertyInt163);
                Assert.AreEqual(o1.PropertyInt164, o2.PropertyInt164);
                Assert.AreEqual(o1.PropertyInt165, o2.PropertyInt165);
                Assert.AreEqual(o1.PropertyInt166, o2.PropertyInt166);
                Assert.AreEqual(o1.PropertyInt167, o2.PropertyInt167);
                Assert.AreEqual(o1.PropertyInt168, o2.PropertyInt168);
                Assert.AreEqual(o1.PropertyInt169, o2.PropertyInt169);
                Assert.AreEqual(o1.PropertyInt1610, o2.PropertyInt1610);
                Assert.AreEqual(o1.PropertyInt1611, o2.PropertyInt1611);
            }
        }
    }

    [Serializable]
    internal class Int16sClass {
        private Int16 _prFieldInt160;
        private Int16 _prFieldInt161;
        private Int16 _prFieldInt162;
        private Int16 _prFieldInt163;
        private Int16 _prFieldInt164;

        public Int16 PrFieldInt160 {
            get { return _prFieldInt160; }
        }

        public Int16 PrFieldInt161 {
            get { return _prFieldInt161; }
        }

        public Int16 PrFieldInt162 {
            get { return _prFieldInt162; }
        }

        public Int16 PrFieldInt163 {
            get { return _prFieldInt163; }
        }

        public Int16 PrFieldInt164 {
            get { return _prFieldInt164; }
        }

        public Int16 _pubFieldInt160;
        public Int16 _pubFieldInt161;
        public Int16 _pubFieldInt162;
        public Int16 _pubFieldInt163;
        public Int16 _pubFieldInt164;
        public Int16 PropertyInt160 { get; set; }
        public Int16 PropertyInt161 { get; set; }
        public Int16 PropertyInt162 { get; set; }
        public Int16 PropertyInt163 { get; set; }
        public Int16 PropertyInt164 { get; set; }
        public Int16 PropertyInt165 { get; set; }
        public Int16 PropertyInt166 { get; set; }
        public Int16 PropertyInt167 { get; set; }
        public Int16 PropertyInt168 { get; set; }
        public Int16 PropertyInt169 { get; set; }
        public Int16 PropertyInt1610 { get; set; }
        public Int16 PropertyInt1611 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldInt160 = Utils.Default<Int16>();
            _prFieldInt161 = Utils.RandomValue<Int16>(r);
            _prFieldInt162 = Utils.Default<Int16>();
            _prFieldInt163 = Utils.RandomValue<Int16>(r);
            _prFieldInt164 = Utils.Default<Int16>();
            _pubFieldInt160 = Utils.Default<Int16>();
            _pubFieldInt161 = Utils.RandomValue<Int16>(r);
            _pubFieldInt162 = Utils.Default<Int16>();
            _pubFieldInt163 = Utils.RandomValue<Int16>(r);
            _pubFieldInt164 = Utils.Default<Int16>();
            PropertyInt160 = Utils.Default<Int16>();
            PropertyInt161 = Utils.RandomValue<Int16>(r);
            PropertyInt162 = Utils.RandomValue<Int16>(r);
            PropertyInt163 = Utils.Default<Int16>();
            PropertyInt164 = Utils.RandomValue<Int16>(r);
            PropertyInt165 = Utils.RandomValue<Int16>(r);
            PropertyInt166 = Utils.Default<Int16>();
            PropertyInt167 = Utils.RandomValue<Int16>(r);
            PropertyInt168 = Utils.RandomValue<Int16>(r);
            PropertyInt169 = Utils.Default<Int16>();
            PropertyInt1610 = Utils.RandomValue<Int16>(r);
            PropertyInt1611 = Utils.RandomValue<Int16>(r);
        }
    }
}