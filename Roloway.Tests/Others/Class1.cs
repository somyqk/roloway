﻿using System;

namespace Roloway.Tests.Others {
    internal class Class1 {}

    [Serializable]
    internal class Class2 {}

    [Serializable]
    internal class Class3 {}

    internal enum SomeEnum {
        Undefined = 0,
        EnumValue1 = 1,
        EnumValue2 = 2,
        EnumValue3 = 3,
        EnumValue4 = 4,
        EnumValue5 = 5,
        EnumValue6 = 6,
        EnumValue7 = 7,
        EnumValue8 = 8,
        EnumValue9 = 9,
        EnumValue10 = 10,
        EnumValue11 = 11,
        EnumValue12 = 12,
        EnumValue13 = 13,
        EnumValue14 = 14,
        EnumValue15 = 15
    }
}