﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Int32_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new Int32sClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (Int32sClass) s2.Deserialize(typeof (Int32sClass));

                Assert.AreEqual(o1.PrFieldInt320, o2.PrFieldInt320);
                Assert.AreEqual(o1.PrFieldInt321, o2.PrFieldInt321);
                Assert.AreEqual(o1.PrFieldInt322, o2.PrFieldInt322);
                Assert.AreEqual(o1.PrFieldInt323, o2.PrFieldInt323);
                Assert.AreEqual(o1.PrFieldInt324, o2.PrFieldInt324);
                Assert.AreEqual(o1._pubFieldInt320, o2._pubFieldInt320);
                Assert.AreEqual(o1._pubFieldInt321, o2._pubFieldInt321);
                Assert.AreEqual(o1._pubFieldInt322, o2._pubFieldInt322);
                Assert.AreEqual(o1._pubFieldInt323, o2._pubFieldInt323);
                Assert.AreEqual(o1._pubFieldInt324, o2._pubFieldInt324);
                Assert.AreEqual(o1.PropertyInt320, o2.PropertyInt320);
                Assert.AreEqual(o1.PropertyInt321, o2.PropertyInt321);
                Assert.AreEqual(o1.PropertyInt322, o2.PropertyInt322);
                Assert.AreEqual(o1.PropertyInt323, o2.PropertyInt323);
                Assert.AreEqual(o1.PropertyInt324, o2.PropertyInt324);
                Assert.AreEqual(o1.PropertyInt325, o2.PropertyInt325);
                Assert.AreEqual(o1.PropertyInt326, o2.PropertyInt326);
                Assert.AreEqual(o1.PropertyInt327, o2.PropertyInt327);
                Assert.AreEqual(o1.PropertyInt328, o2.PropertyInt328);
                Assert.AreEqual(o1.PropertyInt329, o2.PropertyInt329);
                Assert.AreEqual(o1.PropertyInt3210, o2.PropertyInt3210);
                Assert.AreEqual(o1.PropertyInt3211, o2.PropertyInt3211);
            }
        }
    }

    [Serializable]
    internal class Int32sClass {
        private Int32 _prFieldInt320;
        private Int32 _prFieldInt321;
        private Int32 _prFieldInt322;
        private Int32 _prFieldInt323;
        private Int32 _prFieldInt324;

        public Int32 PrFieldInt320 {
            get { return _prFieldInt320; }
        }

        public Int32 PrFieldInt321 {
            get { return _prFieldInt321; }
        }

        public Int32 PrFieldInt322 {
            get { return _prFieldInt322; }
        }

        public Int32 PrFieldInt323 {
            get { return _prFieldInt323; }
        }

        public Int32 PrFieldInt324 {
            get { return _prFieldInt324; }
        }

        public Int32 _pubFieldInt320;
        public Int32 _pubFieldInt321;
        public Int32 _pubFieldInt322;
        public Int32 _pubFieldInt323;
        public Int32 _pubFieldInt324;
        public Int32 PropertyInt320 { get; set; }
        public Int32 PropertyInt321 { get; set; }
        public Int32 PropertyInt322 { get; set; }
        public Int32 PropertyInt323 { get; set; }
        public Int32 PropertyInt324 { get; set; }
        public Int32 PropertyInt325 { get; set; }
        public Int32 PropertyInt326 { get; set; }
        public Int32 PropertyInt327 { get; set; }
        public Int32 PropertyInt328 { get; set; }
        public Int32 PropertyInt329 { get; set; }
        public Int32 PropertyInt3210 { get; set; }
        public Int32 PropertyInt3211 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldInt320 = Utils.Default<Int32>();
            _prFieldInt321 = Utils.RandomValue<Int32>(r);
            _prFieldInt322 = Utils.Default<Int32>();
            _prFieldInt323 = Utils.RandomValue<Int32>(r);
            _prFieldInt324 = Utils.Default<Int32>();
            _pubFieldInt320 = Utils.Default<Int32>();
            _pubFieldInt321 = Utils.RandomValue<Int32>(r);
            _pubFieldInt322 = Utils.Default<Int32>();
            _pubFieldInt323 = Utils.RandomValue<Int32>(r);
            _pubFieldInt324 = Utils.Default<Int32>();
            PropertyInt320 = Utils.Default<Int32>();
            PropertyInt321 = Utils.RandomValue<Int32>(r);
            PropertyInt322 = Utils.RandomValue<Int32>(r);
            PropertyInt323 = Utils.Default<Int32>();
            PropertyInt324 = Utils.RandomValue<Int32>(r);
            PropertyInt325 = Utils.RandomValue<Int32>(r);
            PropertyInt326 = Utils.Default<Int32>();
            PropertyInt327 = Utils.RandomValue<Int32>(r);
            PropertyInt328 = Utils.RandomValue<Int32>(r);
            PropertyInt329 = Utils.Default<Int32>();
            PropertyInt3210 = Utils.RandomValue<Int32>(r);
            PropertyInt3211 = Utils.RandomValue<Int32>(r);
        }
    }
}