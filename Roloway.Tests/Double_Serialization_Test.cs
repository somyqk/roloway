﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Double_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new DoublesClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (DoublesClass) s2.Deserialize(typeof (DoublesClass));

                Assert.AreEqual(o1.PrFieldDouble0, o2.PrFieldDouble0);
                Assert.AreEqual(o1.PrFieldDouble1, o2.PrFieldDouble1);
                Assert.AreEqual(o1.PrFieldDouble2, o2.PrFieldDouble2);
                Assert.AreEqual(o1.PrFieldDouble3, o2.PrFieldDouble3);
                Assert.AreEqual(o1.PrFieldDouble4, o2.PrFieldDouble4);
                Assert.AreEqual(o1._pubFieldDouble0, o2._pubFieldDouble0);
                Assert.AreEqual(o1._pubFieldDouble1, o2._pubFieldDouble1);
                Assert.AreEqual(o1._pubFieldDouble2, o2._pubFieldDouble2);
                Assert.AreEqual(o1._pubFieldDouble3, o2._pubFieldDouble3);
                Assert.AreEqual(o1._pubFieldDouble4, o2._pubFieldDouble4);
                Assert.AreEqual(o1.PropertyDouble0, o2.PropertyDouble0);
                Assert.AreEqual(o1.PropertyDouble1, o2.PropertyDouble1);
                Assert.AreEqual(o1.PropertyDouble2, o2.PropertyDouble2);
                Assert.AreEqual(o1.PropertyDouble3, o2.PropertyDouble3);
                Assert.AreEqual(o1.PropertyDouble4, o2.PropertyDouble4);
                Assert.AreEqual(o1.PropertyDouble5, o2.PropertyDouble5);
                Assert.AreEqual(o1.PropertyDouble6, o2.PropertyDouble6);
                Assert.AreEqual(o1.PropertyDouble7, o2.PropertyDouble7);
                Assert.AreEqual(o1.PropertyDouble8, o2.PropertyDouble8);
                Assert.AreEqual(o1.PropertyDouble9, o2.PropertyDouble9);
                Assert.AreEqual(o1.PropertyDouble10, o2.PropertyDouble10);
                Assert.AreEqual(o1.PropertyDouble11, o2.PropertyDouble11);
            }
        }
    }

    [Serializable]
    internal class DoublesClass {
        private Double _prFieldDouble0;
        private Double _prFieldDouble1;
        private Double _prFieldDouble2;
        private Double _prFieldDouble3;
        private Double _prFieldDouble4;

        public Double PrFieldDouble0 {
            get { return _prFieldDouble0; }
        }

        public Double PrFieldDouble1 {
            get { return _prFieldDouble1; }
        }

        public Double PrFieldDouble2 {
            get { return _prFieldDouble2; }
        }

        public Double PrFieldDouble3 {
            get { return _prFieldDouble3; }
        }

        public Double PrFieldDouble4 {
            get { return _prFieldDouble4; }
        }

        public Double _pubFieldDouble0;
        public Double _pubFieldDouble1;
        public Double _pubFieldDouble2;
        public Double _pubFieldDouble3;
        public Double _pubFieldDouble4;
        public Double PropertyDouble0 { get; set; }
        public Double PropertyDouble1 { get; set; }
        public Double PropertyDouble2 { get; set; }
        public Double PropertyDouble3 { get; set; }
        public Double PropertyDouble4 { get; set; }
        public Double PropertyDouble5 { get; set; }
        public Double PropertyDouble6 { get; set; }
        public Double PropertyDouble7 { get; set; }
        public Double PropertyDouble8 { get; set; }
        public Double PropertyDouble9 { get; set; }
        public Double PropertyDouble10 { get; set; }
        public Double PropertyDouble11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldDouble0 = Utils.Default<Double>();
            _prFieldDouble1 = Utils.RandomValue<Double>(r);
            _prFieldDouble2 = Utils.Default<Double>();
            _prFieldDouble3 = Utils.RandomValue<Double>(r);
            _prFieldDouble4 = Utils.Default<Double>();
            _pubFieldDouble0 = Utils.Default<Double>();
            _pubFieldDouble1 = Utils.RandomValue<Double>(r);
            _pubFieldDouble2 = Utils.Default<Double>();
            _pubFieldDouble3 = Utils.RandomValue<Double>(r);
            _pubFieldDouble4 = Utils.Default<Double>();
            PropertyDouble0 = Utils.Default<Double>();
            PropertyDouble1 = Utils.RandomValue<Double>(r);
            PropertyDouble2 = Utils.RandomValue<Double>(r);
            PropertyDouble3 = Utils.Default<Double>();
            PropertyDouble4 = Utils.RandomValue<Double>(r);
            PropertyDouble5 = Utils.RandomValue<Double>(r);
            PropertyDouble6 = Utils.Default<Double>();
            PropertyDouble7 = Utils.RandomValue<Double>(r);
            PropertyDouble8 = Utils.RandomValue<Double>(r);
            PropertyDouble9 = Utils.Default<Double>();
            PropertyDouble10 = Utils.RandomValue<Double>(r);
            PropertyDouble11 = Utils.RandomValue<Double>(r);
        }
    }
}