﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Struct_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new StructClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (StructClass) s2.Deserialize(typeof (StructClass));

                Assert.AreEqual(o1.PrFieldRandomizableStruct0, o2.PrFieldRandomizableStruct0);
                Assert.AreEqual(o1.PrFieldRandomizableStruct1, o2.PrFieldRandomizableStruct1);
                Assert.AreEqual(o1.PrFieldRandomizableStruct2, o2.PrFieldRandomizableStruct2);
                Assert.AreEqual(o1.PrFieldRandomizableStruct3, o2.PrFieldRandomizableStruct3);
                Assert.AreEqual(o1.PrFieldRandomizableStruct4, o2.PrFieldRandomizableStruct4);
                Assert.AreEqual(o1._pubFieldRandomizableStruct0, o2._pubFieldRandomizableStruct0);
                Assert.AreEqual(o1._pubFieldRandomizableStruct1, o2._pubFieldRandomizableStruct1);
                Assert.AreEqual(o1._pubFieldRandomizableStruct2, o2._pubFieldRandomizableStruct2);
                Assert.AreEqual(o1._pubFieldRandomizableStruct3, o2._pubFieldRandomizableStruct3);
                Assert.AreEqual(o1._pubFieldRandomizableStruct4, o2._pubFieldRandomizableStruct4);
                Assert.AreEqual(o1.PropertyRandomizableStruct0, o2.PropertyRandomizableStruct0);
                Assert.AreEqual(o1.PropertyRandomizableStruct1, o2.PropertyRandomizableStruct1);
                Assert.AreEqual(o1.PropertyRandomizableStruct2, o2.PropertyRandomizableStruct2);
                Assert.AreEqual(o1.PropertyRandomizableStruct3, o2.PropertyRandomizableStruct3);
                Assert.AreEqual(o1.PropertyRandomizableStruct4, o2.PropertyRandomizableStruct4);
                Assert.AreEqual(o1.PropertyRandomizableStruct5, o2.PropertyRandomizableStruct5);
                Assert.AreEqual(o1.PropertyRandomizableStruct6, o2.PropertyRandomizableStruct6);
                Assert.AreEqual(o1.PropertyRandomizableStruct7, o2.PropertyRandomizableStruct7);
                Assert.AreEqual(o1.PropertyRandomizableStruct8, o2.PropertyRandomizableStruct8);
                Assert.AreEqual(o1.PropertyRandomizableStruct9, o2.PropertyRandomizableStruct9);
                Assert.AreEqual(o1.PropertyRandomizableStruct10, o2.PropertyRandomizableStruct10);
                Assert.AreEqual(o1.PropertyRandomizableStruct11, o2.PropertyRandomizableStruct11);
            }
        }
    }

    [Serializable]
    internal class StructClass {
        private RandomizableStruct _prFieldRandomizableStruct0;
        private RandomizableStruct _prFieldRandomizableStruct1;
        private RandomizableStruct _prFieldRandomizableStruct2;
        private RandomizableStruct _prFieldRandomizableStruct3;
        private RandomizableStruct _prFieldRandomizableStruct4;

        public RandomizableStruct PrFieldRandomizableStruct0 {
            get { return _prFieldRandomizableStruct0; }
        }

        public RandomizableStruct PrFieldRandomizableStruct1 {
            get { return _prFieldRandomizableStruct1; }
        }

        public RandomizableStruct PrFieldRandomizableStruct2 {
            get { return _prFieldRandomizableStruct2; }
        }

        public RandomizableStruct PrFieldRandomizableStruct3 {
            get { return _prFieldRandomizableStruct3; }
        }

        public RandomizableStruct PrFieldRandomizableStruct4 {
            get { return _prFieldRandomizableStruct4; }
        }

        public RandomizableStruct _pubFieldRandomizableStruct0;
        public RandomizableStruct _pubFieldRandomizableStruct1;
        public RandomizableStruct _pubFieldRandomizableStruct2;
        public RandomizableStruct _pubFieldRandomizableStruct3;
        public RandomizableStruct _pubFieldRandomizableStruct4;
        public RandomizableStruct PropertyRandomizableStruct0 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct1 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct2 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct3 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct4 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct5 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct6 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct7 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct8 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct9 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct10 { get; set; }
        public RandomizableStruct PropertyRandomizableStruct11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldRandomizableStruct0 = Utils.Default<RandomizableStruct>();
            _prFieldRandomizableStruct1 = Utils.RandomValue<RandomizableStruct>(r);
            _prFieldRandomizableStruct2 = Utils.RandomValue<RandomizableStruct>(r);
            _prFieldRandomizableStruct3 = Utils.Default<RandomizableStruct>();
            _prFieldRandomizableStruct4 = Utils.RandomValue<RandomizableStruct>(r);
            _pubFieldRandomizableStruct0 = Utils.Default<RandomizableStruct>();
            _pubFieldRandomizableStruct1 = Utils.RandomValue<RandomizableStruct>(r);
            _pubFieldRandomizableStruct2 = Utils.Default<RandomizableStruct>();
            _pubFieldRandomizableStruct3 = Utils.RandomValue<RandomizableStruct>(r);
            _pubFieldRandomizableStruct4 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct0 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct1 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct2 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct3 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct4 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct5 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct6 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct7 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct8 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct9 = Utils.Default<RandomizableStruct>();
            PropertyRandomizableStruct10 = Utils.RandomValue<RandomizableStruct>(r);
            PropertyRandomizableStruct11 = Utils.RandomValue<RandomizableStruct>(r);
        }
    }
}