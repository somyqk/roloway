﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Decimal_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new DecimalsClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (DecimalsClass) s2.Deserialize(typeof (DecimalsClass));

                Assert.AreEqual(o1.PrFieldDecimal0, o2.PrFieldDecimal0);
                Assert.AreEqual(o1.PrFieldDecimal1, o2.PrFieldDecimal1);
                Assert.AreEqual(o1.PrFieldDecimal2, o2.PrFieldDecimal2);
                Assert.AreEqual(o1.PrFieldDecimal3, o2.PrFieldDecimal3);
                Assert.AreEqual(o1.PrFieldDecimal4, o2.PrFieldDecimal4);
                Assert.AreEqual(o1._pubFieldDecimal0, o2._pubFieldDecimal0);
                Assert.AreEqual(o1._pubFieldDecimal1, o2._pubFieldDecimal1);
                Assert.AreEqual(o1._pubFieldDecimal2, o2._pubFieldDecimal2);
                Assert.AreEqual(o1._pubFieldDecimal3, o2._pubFieldDecimal3);
                Assert.AreEqual(o1._pubFieldDecimal4, o2._pubFieldDecimal4);
                Assert.AreEqual(o1.PropertyDecimal0, o2.PropertyDecimal0);
                Assert.AreEqual(o1.PropertyDecimal1, o2.PropertyDecimal1);
                Assert.AreEqual(o1.PropertyDecimal2, o2.PropertyDecimal2);
                Assert.AreEqual(o1.PropertyDecimal3, o2.PropertyDecimal3);
                Assert.AreEqual(o1.PropertyDecimal4, o2.PropertyDecimal4);
                Assert.AreEqual(o1.PropertyDecimal5, o2.PropertyDecimal5);
                Assert.AreEqual(o1.PropertyDecimal6, o2.PropertyDecimal6);
                Assert.AreEqual(o1.PropertyDecimal7, o2.PropertyDecimal7);
                Assert.AreEqual(o1.PropertyDecimal8, o2.PropertyDecimal8);
                Assert.AreEqual(o1.PropertyDecimal9, o2.PropertyDecimal9);
                Assert.AreEqual(o1.PropertyDecimal10, o2.PropertyDecimal10);
                Assert.AreEqual(o1.PropertyDecimal11, o2.PropertyDecimal11);
            }
        }
    }

    [Serializable]
    internal class DecimalsClass {
        private Decimal _prFieldDecimal0;
        private Decimal _prFieldDecimal1;
        private Decimal _prFieldDecimal2;
        private Decimal _prFieldDecimal3;
        private Decimal _prFieldDecimal4;

        public Decimal PrFieldDecimal0 {
            get { return _prFieldDecimal0; }
        }

        public Decimal PrFieldDecimal1 {
            get { return _prFieldDecimal1; }
        }

        public Decimal PrFieldDecimal2 {
            get { return _prFieldDecimal2; }
        }

        public Decimal PrFieldDecimal3 {
            get { return _prFieldDecimal3; }
        }

        public Decimal PrFieldDecimal4 {
            get { return _prFieldDecimal4; }
        }

        public Decimal _pubFieldDecimal0;
        public Decimal _pubFieldDecimal1;
        public Decimal _pubFieldDecimal2;
        public Decimal _pubFieldDecimal3;
        public Decimal _pubFieldDecimal4;
        public Decimal PropertyDecimal0 { get; set; }
        public Decimal PropertyDecimal1 { get; set; }
        public Decimal PropertyDecimal2 { get; set; }
        public Decimal PropertyDecimal3 { get; set; }
        public Decimal PropertyDecimal4 { get; set; }
        public Decimal PropertyDecimal5 { get; set; }
        public Decimal PropertyDecimal6 { get; set; }
        public Decimal PropertyDecimal7 { get; set; }
        public Decimal PropertyDecimal8 { get; set; }
        public Decimal PropertyDecimal9 { get; set; }
        public Decimal PropertyDecimal10 { get; set; }
        public Decimal PropertyDecimal11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldDecimal0 = Utils.Default<Decimal>();
            _prFieldDecimal1 = Utils.RandomValue<Decimal>(r);
            _prFieldDecimal2 = Utils.Default<Decimal>();
            _prFieldDecimal3 = Utils.RandomValue<Decimal>(r);
            _prFieldDecimal4 = Utils.Default<Decimal>();
            _pubFieldDecimal0 = Utils.Default<Decimal>();
            _pubFieldDecimal1 = Utils.RandomValue<Decimal>(r);
            _pubFieldDecimal2 = Utils.Default<Decimal>();
            _pubFieldDecimal3 = Utils.RandomValue<Decimal>(r);
            _pubFieldDecimal4 = Utils.Default<Decimal>();
            PropertyDecimal0 = Utils.Default<Decimal>();
            PropertyDecimal1 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal2 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal3 = Utils.Default<Decimal>();
            PropertyDecimal4 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal5 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal6 = Utils.Default<Decimal>();
            PropertyDecimal7 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal8 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal9 = Utils.Default<Decimal>();
            PropertyDecimal10 = Utils.RandomValue<Decimal>(r);
            PropertyDecimal11 = Utils.RandomValue<Decimal>(r);
        }
    }
}