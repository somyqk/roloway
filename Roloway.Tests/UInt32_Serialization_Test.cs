﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class UInt32_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new UInt32sClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (UInt32sClass) s2.Deserialize(typeof (UInt32sClass));

                Assert.AreEqual(o1.PrFieldUInt320, o2.PrFieldUInt320);
                Assert.AreEqual(o1.PrFieldUInt321, o2.PrFieldUInt321);
                Assert.AreEqual(o1.PrFieldUInt322, o2.PrFieldUInt322);
                Assert.AreEqual(o1.PrFieldUInt323, o2.PrFieldUInt323);
                Assert.AreEqual(o1.PrFieldUInt324, o2.PrFieldUInt324);
                Assert.AreEqual(o1._pubFieldUInt320, o2._pubFieldUInt320);
                Assert.AreEqual(o1._pubFieldUInt321, o2._pubFieldUInt321);
                Assert.AreEqual(o1._pubFieldUInt322, o2._pubFieldUInt322);
                Assert.AreEqual(o1._pubFieldUInt323, o2._pubFieldUInt323);
                Assert.AreEqual(o1._pubFieldUInt324, o2._pubFieldUInt324);
                Assert.AreEqual(o1.PropertyUInt320, o2.PropertyUInt320);
                Assert.AreEqual(o1.PropertyUInt321, o2.PropertyUInt321);
                Assert.AreEqual(o1.PropertyUInt322, o2.PropertyUInt322);
                Assert.AreEqual(o1.PropertyUInt323, o2.PropertyUInt323);
                Assert.AreEqual(o1.PropertyUInt324, o2.PropertyUInt324);
                Assert.AreEqual(o1.PropertyUInt325, o2.PropertyUInt325);
                Assert.AreEqual(o1.PropertyUInt326, o2.PropertyUInt326);
                Assert.AreEqual(o1.PropertyUInt327, o2.PropertyUInt327);
                Assert.AreEqual(o1.PropertyUInt328, o2.PropertyUInt328);
                Assert.AreEqual(o1.PropertyUInt329, o2.PropertyUInt329);
                Assert.AreEqual(o1.PropertyUInt3210, o2.PropertyUInt3210);
                Assert.AreEqual(o1.PropertyUInt3211, o2.PropertyUInt3211);
            }
        }
    }

    [Serializable]
    internal class UInt32sClass {
        private UInt32 _prFieldUInt320;
        private UInt32 _prFieldUInt321;
        private UInt32 _prFieldUInt322;
        private UInt32 _prFieldUInt323;
        private UInt32 _prFieldUInt324;

        public UInt32 PrFieldUInt320 {
            get { return _prFieldUInt320; }
        }

        public UInt32 PrFieldUInt321 {
            get { return _prFieldUInt321; }
        }

        public UInt32 PrFieldUInt322 {
            get { return _prFieldUInt322; }
        }

        public UInt32 PrFieldUInt323 {
            get { return _prFieldUInt323; }
        }

        public UInt32 PrFieldUInt324 {
            get { return _prFieldUInt324; }
        }

        public UInt32 _pubFieldUInt320;
        public UInt32 _pubFieldUInt321;
        public UInt32 _pubFieldUInt322;
        public UInt32 _pubFieldUInt323;
        public UInt32 _pubFieldUInt324;
        public UInt32 PropertyUInt320 { get; set; }
        public UInt32 PropertyUInt321 { get; set; }
        public UInt32 PropertyUInt322 { get; set; }
        public UInt32 PropertyUInt323 { get; set; }
        public UInt32 PropertyUInt324 { get; set; }
        public UInt32 PropertyUInt325 { get; set; }
        public UInt32 PropertyUInt326 { get; set; }
        public UInt32 PropertyUInt327 { get; set; }
        public UInt32 PropertyUInt328 { get; set; }
        public UInt32 PropertyUInt329 { get; set; }
        public UInt32 PropertyUInt3210 { get; set; }
        public UInt32 PropertyUInt3211 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldUInt320 = Utils.Default<UInt32>();
            _prFieldUInt321 = Utils.RandomValue<UInt32>(r);
            _prFieldUInt322 = Utils.Default<UInt32>();
            _prFieldUInt323 = Utils.RandomValue<UInt32>(r);
            _prFieldUInt324 = Utils.Default<UInt32>();
            _pubFieldUInt320 = Utils.Default<UInt32>();
            _pubFieldUInt321 = Utils.RandomValue<UInt32>(r);
            _pubFieldUInt322 = Utils.Default<UInt32>();
            _pubFieldUInt323 = Utils.RandomValue<UInt32>(r);
            _pubFieldUInt324 = Utils.Default<UInt32>();
            PropertyUInt320 = Utils.Default<UInt32>();
            PropertyUInt321 = Utils.RandomValue<UInt32>(r);
            PropertyUInt322 = Utils.RandomValue<UInt32>(r);
            PropertyUInt323 = Utils.Default<UInt32>();
            PropertyUInt324 = Utils.RandomValue<UInt32>(r);
            PropertyUInt325 = Utils.RandomValue<UInt32>(r);
            PropertyUInt326 = Utils.Default<UInt32>();
            PropertyUInt327 = Utils.RandomValue<UInt32>(r);
            PropertyUInt328 = Utils.RandomValue<UInt32>(r);
            PropertyUInt329 = Utils.Default<UInt32>();
            PropertyUInt3210 = Utils.RandomValue<UInt32>(r);
            PropertyUInt3211 = Utils.RandomValue<UInt32>(r);
        }
    }
}