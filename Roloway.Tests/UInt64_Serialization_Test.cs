﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class UInt64_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new UInt64sClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (UInt64sClass) s2.Deserialize(typeof (UInt64sClass));

                Assert.AreEqual(o1.PrFieldUInt640, o2.PrFieldUInt640);
                Assert.AreEqual(o1.PrFieldUInt641, o2.PrFieldUInt641);
                Assert.AreEqual(o1.PrFieldUInt642, o2.PrFieldUInt642);
                Assert.AreEqual(o1.PrFieldUInt643, o2.PrFieldUInt643);
                Assert.AreEqual(o1.PrFieldUInt644, o2.PrFieldUInt644);
                Assert.AreEqual(o1._pubFieldUInt640, o2._pubFieldUInt640);
                Assert.AreEqual(o1._pubFieldUInt641, o2._pubFieldUInt641);
                Assert.AreEqual(o1._pubFieldUInt642, o2._pubFieldUInt642);
                Assert.AreEqual(o1._pubFieldUInt643, o2._pubFieldUInt643);
                Assert.AreEqual(o1._pubFieldUInt644, o2._pubFieldUInt644);
                Assert.AreEqual(o1.PropertyUInt640, o2.PropertyUInt640);
                Assert.AreEqual(o1.PropertyUInt641, o2.PropertyUInt641);
                Assert.AreEqual(o1.PropertyUInt642, o2.PropertyUInt642);
                Assert.AreEqual(o1.PropertyUInt643, o2.PropertyUInt643);
                Assert.AreEqual(o1.PropertyUInt644, o2.PropertyUInt644);
                Assert.AreEqual(o1.PropertyUInt645, o2.PropertyUInt645);
                Assert.AreEqual(o1.PropertyUInt646, o2.PropertyUInt646);
                Assert.AreEqual(o1.PropertyUInt647, o2.PropertyUInt647);
                Assert.AreEqual(o1.PropertyUInt648, o2.PropertyUInt648);
                Assert.AreEqual(o1.PropertyUInt649, o2.PropertyUInt649);
                Assert.AreEqual(o1.PropertyUInt6410, o2.PropertyUInt6410);
                Assert.AreEqual(o1.PropertyUInt6411, o2.PropertyUInt6411);
            }
        }
    }

    [Serializable]
    internal class UInt64sClass {
        private UInt64 _prFieldUInt640;
        private UInt64 _prFieldUInt641;
        private UInt64 _prFieldUInt642;
        private UInt64 _prFieldUInt643;
        private UInt64 _prFieldUInt644;

        public UInt64 PrFieldUInt640 {
            get { return _prFieldUInt640; }
        }

        public UInt64 PrFieldUInt641 {
            get { return _prFieldUInt641; }
        }

        public UInt64 PrFieldUInt642 {
            get { return _prFieldUInt642; }
        }

        public UInt64 PrFieldUInt643 {
            get { return _prFieldUInt643; }
        }

        public UInt64 PrFieldUInt644 {
            get { return _prFieldUInt644; }
        }

        public UInt64 _pubFieldUInt640;
        public UInt64 _pubFieldUInt641;
        public UInt64 _pubFieldUInt642;
        public UInt64 _pubFieldUInt643;
        public UInt64 _pubFieldUInt644;
        public UInt64 PropertyUInt640 { get; set; }
        public UInt64 PropertyUInt641 { get; set; }
        public UInt64 PropertyUInt642 { get; set; }
        public UInt64 PropertyUInt643 { get; set; }
        public UInt64 PropertyUInt644 { get; set; }
        public UInt64 PropertyUInt645 { get; set; }
        public UInt64 PropertyUInt646 { get; set; }
        public UInt64 PropertyUInt647 { get; set; }
        public UInt64 PropertyUInt648 { get; set; }
        public UInt64 PropertyUInt649 { get; set; }
        public UInt64 PropertyUInt6410 { get; set; }
        public UInt64 PropertyUInt6411 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldUInt640 = Utils.Default<UInt64>();
            _prFieldUInt641 = Utils.RandomValue<UInt64>(r);
            _prFieldUInt642 = Utils.Default<UInt64>();
            _prFieldUInt643 = Utils.RandomValue<UInt64>(r);
            _prFieldUInt644 = Utils.Default<UInt64>();
            _pubFieldUInt640 = Utils.Default<UInt64>();
            _pubFieldUInt641 = Utils.RandomValue<UInt64>(r);
            _pubFieldUInt642 = Utils.Default<UInt64>();
            _pubFieldUInt643 = Utils.RandomValue<UInt64>(r);
            _pubFieldUInt644 = Utils.Default<UInt64>();
            PropertyUInt640 = Utils.Default<UInt64>();
            PropertyUInt641 = Utils.RandomValue<UInt64>(r);
            PropertyUInt642 = Utils.RandomValue<UInt64>(r);
            PropertyUInt643 = Utils.Default<UInt64>();
            PropertyUInt644 = Utils.RandomValue<UInt64>(r);
            PropertyUInt645 = Utils.RandomValue<UInt64>(r);
            PropertyUInt646 = Utils.Default<UInt64>();
            PropertyUInt647 = Utils.RandomValue<UInt64>(r);
            PropertyUInt648 = Utils.RandomValue<UInt64>(r);
            PropertyUInt649 = Utils.Default<UInt64>();
            PropertyUInt6410 = Utils.RandomValue<UInt64>(r);
            PropertyUInt6411 = Utils.RandomValue<UInt64>(r);
        }
    }
}