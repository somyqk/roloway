﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class String_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new StringsClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (StringsClass) s2.Deserialize(typeof (StringsClass));

                Assert.AreEqual(o1.PrFieldString0, o2.PrFieldString0);
                Assert.AreEqual(o1.PrFieldString1, o2.PrFieldString1);
                Assert.AreEqual(o1.PrFieldString2, o2.PrFieldString2);
                Assert.AreEqual(o1.PrFieldString3, o2.PrFieldString3);
                Assert.AreEqual(o1.PrFieldString4, o2.PrFieldString4);
                Assert.AreEqual(o1._pubFieldString0, o2._pubFieldString0);
                Assert.AreEqual(o1._pubFieldString1, o2._pubFieldString1);
                Assert.AreEqual(o1._pubFieldString2, o2._pubFieldString2);
                Assert.AreEqual(o1._pubFieldString3, o2._pubFieldString3);
                Assert.AreEqual(o1._pubFieldString4, o2._pubFieldString4);
                Assert.AreEqual(o1.PropertyString0, o2.PropertyString0);
                Assert.AreEqual(o1.PropertyString1, o2.PropertyString1);
                Assert.AreEqual(o1.PropertyString2, o2.PropertyString2);
                Assert.AreEqual(o1.PropertyString3, o2.PropertyString3);
                Assert.AreEqual(o1.PropertyString4, o2.PropertyString4);
                Assert.AreEqual(o1.PropertyString5, o2.PropertyString5);
                Assert.AreEqual(o1.PropertyString6, o2.PropertyString6);
                Assert.AreEqual(o1.PropertyString7, o2.PropertyString7);
                Assert.AreEqual(o1.PropertyString8, o2.PropertyString8);
                Assert.AreEqual(o1.PropertyString9, o2.PropertyString9);
                Assert.AreEqual(o1.PropertyString10, o2.PropertyString10);
                Assert.AreEqual(o1.PropertyString11, o2.PropertyString11);
            }
        }
    }

    [Serializable]
    internal class StringsClass {
        private String _prFieldString0;
        private String _prFieldString1;
        private String _prFieldString2;
        private String _prFieldString3;
        private String _prFieldString4;

        public String PrFieldString0 {
            get { return _prFieldString0; }
        }

        public String PrFieldString1 {
            get { return _prFieldString1; }
        }

        public String PrFieldString2 {
            get { return _prFieldString2; }
        }

        public String PrFieldString3 {
            get { return _prFieldString3; }
        }

        public String PrFieldString4 {
            get { return _prFieldString4; }
        }

        public String _pubFieldString0;
        public String _pubFieldString1;
        public String _pubFieldString2;
        public String _pubFieldString3;
        public String _pubFieldString4;
        public String PropertyString0 { get; set; }
        public String PropertyString1 { get; set; }
        public String PropertyString2 { get; set; }
        public String PropertyString3 { get; set; }
        public String PropertyString4 { get; set; }
        public String PropertyString5 { get; set; }
        public String PropertyString6 { get; set; }
        public String PropertyString7 { get; set; }
        public String PropertyString8 { get; set; }
        public String PropertyString9 { get; set; }
        public String PropertyString10 { get; set; }
        public String PropertyString11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldString0 = Utils.Default<String>();
            _prFieldString1 = Utils.RandomValue<String>(r);
            _prFieldString2 = string.Empty;
            _prFieldString3 = Utils.RandomValue<String>(r);
            _prFieldString4 = Utils.Default<String>();
            _pubFieldString0 = Utils.Default<String>();
            _pubFieldString1 = Utils.RandomValue<String>(r);
            _pubFieldString2 = string.Empty;
            _pubFieldString3 = Utils.RandomValue<String>(r);
            _pubFieldString4 = Utils.Default<String>();
            PropertyString0 = Utils.Default<String>();
            PropertyString1 = Utils.RandomValue<String>(r);
            PropertyString2 = Utils.RandomValue<String>(r);
            PropertyString3 = Utils.Default<String>();
            PropertyString4 = Utils.RandomValue<String>(r);
            PropertyString5 = Utils.RandomValue<String>(r);
            PropertyString6 = string.Empty;
            PropertyString7 = Utils.RandomValue<String>(r);
            PropertyString8 = Utils.RandomValue<String>(r);
            PropertyString9 = Utils.Default<String>();
            PropertyString10 = Utils.RandomValue<String>(r);
            PropertyString11 = Utils.RandomValue<String>(r);
        }
    }
}