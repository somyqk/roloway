﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Boolean_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new BooleansClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (BooleansClass) s2.Deserialize(typeof (BooleansClass));

                Assert.AreEqual(o1.PrFieldBoolean0, o2.PrFieldBoolean0);
                Assert.AreEqual(o1.PrFieldBoolean1, o2.PrFieldBoolean1);
                Assert.AreEqual(o1.PrFieldBoolean2, o2.PrFieldBoolean2);
                Assert.AreEqual(o1.PrFieldBoolean3, o2.PrFieldBoolean3);
                Assert.AreEqual(o1.PrFieldBoolean4, o2.PrFieldBoolean4);
                Assert.AreEqual(o1._pubFieldBoolean0, o2._pubFieldBoolean0);
                Assert.AreEqual(o1._pubFieldBoolean1, o2._pubFieldBoolean1);
                Assert.AreEqual(o1._pubFieldBoolean2, o2._pubFieldBoolean2);
                Assert.AreEqual(o1._pubFieldBoolean3, o2._pubFieldBoolean3);
                Assert.AreEqual(o1._pubFieldBoolean4, o2._pubFieldBoolean4);
                Assert.AreEqual(o1.PropertyBoolean0, o2.PropertyBoolean0);
                Assert.AreEqual(o1.PropertyBoolean1, o2.PropertyBoolean1);
                Assert.AreEqual(o1.PropertyBoolean2, o2.PropertyBoolean2);
                Assert.AreEqual(o1.PropertyBoolean3, o2.PropertyBoolean3);
                Assert.AreEqual(o1.PropertyBoolean4, o2.PropertyBoolean4);
                Assert.AreEqual(o1.PropertyBoolean5, o2.PropertyBoolean5);
                Assert.AreEqual(o1.PropertyBoolean6, o2.PropertyBoolean6);
                Assert.AreEqual(o1.PropertyBoolean7, o2.PropertyBoolean7);
                Assert.AreEqual(o1.PropertyBoolean8, o2.PropertyBoolean8);
                Assert.AreEqual(o1.PropertyBoolean9, o2.PropertyBoolean9);
                Assert.AreEqual(o1.PropertyBoolean10, o2.PropertyBoolean10);
                Assert.AreEqual(o1.PropertyBoolean11, o2.PropertyBoolean11);
            }
        }
    }

    [Serializable]
    internal class BooleansClass {
        private Boolean _prFieldBoolean0;
        private Boolean _prFieldBoolean1;
        private Boolean _prFieldBoolean2;
        private Boolean _prFieldBoolean3;
        private Boolean _prFieldBoolean4;

        public Boolean PrFieldBoolean0 {
            get { return _prFieldBoolean0; }
        }

        public Boolean PrFieldBoolean1 {
            get { return _prFieldBoolean1; }
        }

        public Boolean PrFieldBoolean2 {
            get { return _prFieldBoolean2; }
        }

        public Boolean PrFieldBoolean3 {
            get { return _prFieldBoolean3; }
        }

        public Boolean PrFieldBoolean4 {
            get { return _prFieldBoolean4; }
        }

        public Boolean _pubFieldBoolean0;
        public Boolean _pubFieldBoolean1;
        public Boolean _pubFieldBoolean2;
        public Boolean _pubFieldBoolean3;
        public Boolean _pubFieldBoolean4;
        public Boolean PropertyBoolean0 { get; set; }
        public Boolean PropertyBoolean1 { get; set; }
        public Boolean PropertyBoolean2 { get; set; }
        public Boolean PropertyBoolean3 { get; set; }
        public Boolean PropertyBoolean4 { get; set; }
        public Boolean PropertyBoolean5 { get; set; }
        public Boolean PropertyBoolean6 { get; set; }
        public Boolean PropertyBoolean7 { get; set; }
        public Boolean PropertyBoolean8 { get; set; }
        public Boolean PropertyBoolean9 { get; set; }
        public Boolean PropertyBoolean10 { get; set; }
        public Boolean PropertyBoolean11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldBoolean0 = Utils.Default<Boolean>();
            _prFieldBoolean1 = Utils.RandomValue<Boolean>(r);
            _prFieldBoolean2 = Utils.Default<Boolean>();
            _prFieldBoolean3 = Utils.RandomValue<Boolean>(r);
            _prFieldBoolean4 = Utils.Default<Boolean>();
            _pubFieldBoolean0 = Utils.Default<Boolean>();
            _pubFieldBoolean1 = Utils.RandomValue<Boolean>(r);
            _pubFieldBoolean2 = Utils.Default<Boolean>();
            _pubFieldBoolean3 = Utils.RandomValue<Boolean>(r);
            _pubFieldBoolean4 = Utils.Default<Boolean>();
            PropertyBoolean0 = Utils.Default<Boolean>();
            PropertyBoolean1 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean2 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean3 = Utils.Default<Boolean>();
            PropertyBoolean4 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean5 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean6 = Utils.Default<Boolean>();
            PropertyBoolean7 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean8 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean9 = Utils.Default<Boolean>();
            PropertyBoolean10 = Utils.RandomValue<Boolean>(r);
            PropertyBoolean11 = Utils.RandomValue<Boolean>(r);
        }
    }
}