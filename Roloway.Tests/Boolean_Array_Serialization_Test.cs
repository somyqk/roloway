﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Boolean_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new BooleanArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (BooleanArrayClass) s2.Deserialize(typeof (BooleanArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldBooleanArr0, o2.PrFieldBooleanArr0);
                CollectionAssert.AreEqual(o1.PrFieldBooleanArr1, o2.PrFieldBooleanArr1);
                CollectionAssert.AreEqual(o1.PrFieldBooleanArr2, o2.PrFieldBooleanArr2);
                CollectionAssert.AreEqual(o1.PrFieldBooleanArr3, o2.PrFieldBooleanArr3);
                CollectionAssert.AreEqual(o1.PrFieldBooleanArr4, o2.PrFieldBooleanArr4);
                CollectionAssert.AreEqual(o1._pubFieldBooleanArr0, o2._pubFieldBooleanArr0);
                CollectionAssert.AreEqual(o1._pubFieldBooleanArr1, o2._pubFieldBooleanArr1);
                CollectionAssert.AreEqual(o1._pubFieldBooleanArr2, o2._pubFieldBooleanArr2);
                CollectionAssert.AreEqual(o1._pubFieldBooleanArr3, o2._pubFieldBooleanArr3);
                CollectionAssert.AreEqual(o1._pubFieldBooleanArr4, o2._pubFieldBooleanArr4);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr0, o2.PropertyBooleanArr0);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr1, o2.PropertyBooleanArr1);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr2, o2.PropertyBooleanArr2);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr3, o2.PropertyBooleanArr3);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr4, o2.PropertyBooleanArr4);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr5, o2.PropertyBooleanArr5);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr6, o2.PropertyBooleanArr6);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr7, o2.PropertyBooleanArr7);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr8, o2.PropertyBooleanArr8);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr9, o2.PropertyBooleanArr9);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr10, o2.PropertyBooleanArr10);
                CollectionAssert.AreEqual(o1.PropertyBooleanArr11, o2.PropertyBooleanArr11);
            }
        }
    }

    [Serializable]
    internal class BooleanArrayClass {
        private Boolean[] _prFieldBooleanArr0;
        private Boolean[] _prFieldBooleanArr1;
        private Boolean[] _prFieldBooleanArr2;
        private Boolean[] _prFieldBooleanArr3;
        private Boolean[] _prFieldBooleanArr4;

        public Boolean[] PrFieldBooleanArr0 {
            get { return _prFieldBooleanArr0; }
        }

        public Boolean[] PrFieldBooleanArr1 {
            get { return _prFieldBooleanArr1; }
        }

        public Boolean[] PrFieldBooleanArr2 {
            get { return _prFieldBooleanArr2; }
        }

        public Boolean[] PrFieldBooleanArr3 {
            get { return _prFieldBooleanArr3; }
        }

        public Boolean[] PrFieldBooleanArr4 {
            get { return _prFieldBooleanArr4; }
        }

        public Boolean[] _pubFieldBooleanArr0;
        public Boolean[] _pubFieldBooleanArr1;
        public Boolean[] _pubFieldBooleanArr2;
        public Boolean[] _pubFieldBooleanArr3;
        public Boolean[] _pubFieldBooleanArr4;
        public Boolean[] PropertyBooleanArr0 { get; set; }
        public Boolean[] PropertyBooleanArr1 { get; set; }
        public Boolean[] PropertyBooleanArr2 { get; set; }
        public Boolean[] PropertyBooleanArr3 { get; set; }
        public Boolean[] PropertyBooleanArr4 { get; set; }
        public Boolean[] PropertyBooleanArr5 { get; set; }
        public Boolean[] PropertyBooleanArr6 { get; set; }
        public Boolean[] PropertyBooleanArr7 { get; set; }
        public Boolean[] PropertyBooleanArr8 { get; set; }
        public Boolean[] PropertyBooleanArr9 { get; set; }
        public Boolean[] PropertyBooleanArr10 { get; set; }
        public Boolean[] PropertyBooleanArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldBooleanArr0 = Utils.Default<Boolean[]>();
            _prFieldBooleanArr1 = Utils.RandomArray<Boolean>(r);
            _prFieldBooleanArr2 = Utils.Default<Boolean[]>();
            _prFieldBooleanArr3 = Utils.EmptyArray<Boolean>();
            _prFieldBooleanArr4 = Utils.Default<Boolean[]>();
            _pubFieldBooleanArr0 = Utils.Default<Boolean[]>();
            _pubFieldBooleanArr1 = Utils.RandomArray<Boolean>(r);
            _pubFieldBooleanArr2 = Utils.Default<Boolean[]>();
            _pubFieldBooleanArr3 = Utils.EmptyArray<Boolean>();
            _pubFieldBooleanArr4 = Utils.Default<Boolean[]>();
            PropertyBooleanArr0 = Utils.Default<Boolean[]>();
            PropertyBooleanArr1 = Utils.RandomArray<Boolean>(r);
            PropertyBooleanArr2 = Utils.Default<Boolean[]>();
            PropertyBooleanArr3 = Utils.EmptyArray<Boolean>();
            PropertyBooleanArr4 = Utils.Default<Boolean[]>();
            PropertyBooleanArr5 = Utils.RandomArray<Boolean>(r);
            PropertyBooleanArr6 = Utils.Default<Boolean[]>();
            PropertyBooleanArr7 = Utils.RandomArray<Boolean>(r);
            PropertyBooleanArr8 = Utils.Default<Boolean[]>();
            PropertyBooleanArr9 = Utils.EmptyArray<Boolean>();
            PropertyBooleanArr10 = Utils.Default<Boolean[]>();
            PropertyBooleanArr11 = Utils.RandomArray<Boolean>(r);
        }
    }
}