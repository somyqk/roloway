﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class UInt32_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new UInt32ArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (UInt32ArrayClass) s2.Deserialize(typeof (UInt32ArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldUInt32Arr0, o2.PrFieldUInt32Arr0);
                CollectionAssert.AreEqual(o1.PrFieldUInt32Arr1, o2.PrFieldUInt32Arr1);
                CollectionAssert.AreEqual(o1.PrFieldUInt32Arr2, o2.PrFieldUInt32Arr2);
                CollectionAssert.AreEqual(o1.PrFieldUInt32Arr3, o2.PrFieldUInt32Arr3);
                CollectionAssert.AreEqual(o1.PrFieldUInt32Arr4, o2.PrFieldUInt32Arr4);
                CollectionAssert.AreEqual(o1._pubFieldUInt32Arr0, o2._pubFieldUInt32Arr0);
                CollectionAssert.AreEqual(o1._pubFieldUInt32Arr1, o2._pubFieldUInt32Arr1);
                CollectionAssert.AreEqual(o1._pubFieldUInt32Arr2, o2._pubFieldUInt32Arr2);
                CollectionAssert.AreEqual(o1._pubFieldUInt32Arr3, o2._pubFieldUInt32Arr3);
                CollectionAssert.AreEqual(o1._pubFieldUInt32Arr4, o2._pubFieldUInt32Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr0, o2.PropertyUInt32Arr0);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr1, o2.PropertyUInt32Arr1);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr2, o2.PropertyUInt32Arr2);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr3, o2.PropertyUInt32Arr3);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr4, o2.PropertyUInt32Arr4);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr5, o2.PropertyUInt32Arr5);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr6, o2.PropertyUInt32Arr6);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr7, o2.PropertyUInt32Arr7);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr8, o2.PropertyUInt32Arr8);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr9, o2.PropertyUInt32Arr9);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr10, o2.PropertyUInt32Arr10);
                CollectionAssert.AreEqual(o1.PropertyUInt32Arr11, o2.PropertyUInt32Arr11);
            }
        }
    }

    [Serializable]
    internal class UInt32ArrayClass {
        private UInt32[] _prFieldUInt32Arr0;
        private UInt32[] _prFieldUInt32Arr1;
        private UInt32[] _prFieldUInt32Arr2;
        private UInt32[] _prFieldUInt32Arr3;
        private UInt32[] _prFieldUInt32Arr4;

        public UInt32[] PrFieldUInt32Arr0 {
            get { return _prFieldUInt32Arr0; }
        }

        public UInt32[] PrFieldUInt32Arr1 {
            get { return _prFieldUInt32Arr1; }
        }

        public UInt32[] PrFieldUInt32Arr2 {
            get { return _prFieldUInt32Arr2; }
        }

        public UInt32[] PrFieldUInt32Arr3 {
            get { return _prFieldUInt32Arr3; }
        }

        public UInt32[] PrFieldUInt32Arr4 {
            get { return _prFieldUInt32Arr4; }
        }

        public UInt32[] _pubFieldUInt32Arr0;
        public UInt32[] _pubFieldUInt32Arr1;
        public UInt32[] _pubFieldUInt32Arr2;
        public UInt32[] _pubFieldUInt32Arr3;
        public UInt32[] _pubFieldUInt32Arr4;
        public UInt32[] PropertyUInt32Arr0 { get; set; }
        public UInt32[] PropertyUInt32Arr1 { get; set; }
        public UInt32[] PropertyUInt32Arr2 { get; set; }
        public UInt32[] PropertyUInt32Arr3 { get; set; }
        public UInt32[] PropertyUInt32Arr4 { get; set; }
        public UInt32[] PropertyUInt32Arr5 { get; set; }
        public UInt32[] PropertyUInt32Arr6 { get; set; }
        public UInt32[] PropertyUInt32Arr7 { get; set; }
        public UInt32[] PropertyUInt32Arr8 { get; set; }
        public UInt32[] PropertyUInt32Arr9 { get; set; }
        public UInt32[] PropertyUInt32Arr10 { get; set; }
        public UInt32[] PropertyUInt32Arr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldUInt32Arr0 = Utils.Default<UInt32[]>();
            _prFieldUInt32Arr1 = Utils.RandomArray<UInt32>(r);
            _prFieldUInt32Arr2 = Utils.Default<UInt32[]>();
            _prFieldUInt32Arr3 = Utils.EmptyArray<UInt32>();
            _prFieldUInt32Arr4 = Utils.Default<UInt32[]>();
            _pubFieldUInt32Arr0 = Utils.Default<UInt32[]>();
            _pubFieldUInt32Arr1 = Utils.RandomArray<UInt32>(r);
            _pubFieldUInt32Arr2 = Utils.Default<UInt32[]>();
            _pubFieldUInt32Arr3 = Utils.EmptyArray<UInt32>();
            _pubFieldUInt32Arr4 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr0 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr1 = Utils.RandomArray<UInt32>(r);
            PropertyUInt32Arr2 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr3 = Utils.EmptyArray<UInt32>();
            PropertyUInt32Arr4 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr5 = Utils.RandomArray<UInt32>(r);
            PropertyUInt32Arr6 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr7 = Utils.RandomArray<UInt32>(r);
            PropertyUInt32Arr8 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr9 = Utils.EmptyArray<UInt32>();
            PropertyUInt32Arr10 = Utils.Default<UInt32[]>();
            PropertyUInt32Arr11 = Utils.RandomArray<UInt32>(r);
        }
    }
}