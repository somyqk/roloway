using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class RandomizableStruct_List_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ListRandomizableStructClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ListRandomizableStructClass) s2.Deserialize(typeof (ListRandomizableStructClass));

                CollectionAssert.AreEqual(o1.PrFieldListList10, o2.PrFieldListList10);
                CollectionAssert.AreEqual(o1.PrFieldListList11, o2.PrFieldListList11);
                CollectionAssert.AreEqual(o1.PrFieldListList12, o2.PrFieldListList12);
                CollectionAssert.AreEqual(o1.PrFieldListList13, o2.PrFieldListList13);
                CollectionAssert.AreEqual(o1.PrFieldListList14, o2.PrFieldListList14);
                CollectionAssert.AreEqual(o1._pubFieldListList10, o2._pubFieldListList10);
                CollectionAssert.AreEqual(o1._pubFieldListList11, o2._pubFieldListList11);
                CollectionAssert.AreEqual(o1._pubFieldListList12, o2._pubFieldListList12);
                CollectionAssert.AreEqual(o1._pubFieldListList13, o2._pubFieldListList13);
                CollectionAssert.AreEqual(o1._pubFieldListList14, o2._pubFieldListList14);
                CollectionAssert.AreEqual(o1.PropertyListList10, o2.PropertyListList10);
                CollectionAssert.AreEqual(o1.PropertyListList11, o2.PropertyListList11);
                CollectionAssert.AreEqual(o1.PropertyListList12, o2.PropertyListList12);
                CollectionAssert.AreEqual(o1.PropertyListList13, o2.PropertyListList13);
                CollectionAssert.AreEqual(o1.PropertyListList14, o2.PropertyListList14);
                CollectionAssert.AreEqual(o1.PropertyListList15, o2.PropertyListList15);
                CollectionAssert.AreEqual(o1.PropertyListList16, o2.PropertyListList16);
                CollectionAssert.AreEqual(o1.PropertyListList17, o2.PropertyListList17);
                CollectionAssert.AreEqual(o1.PropertyListList18, o2.PropertyListList18);
                CollectionAssert.AreEqual(o1.PropertyListList19, o2.PropertyListList19);
                CollectionAssert.AreEqual(o1.PropertyListList110, o2.PropertyListList110);
                CollectionAssert.AreEqual(o1.PropertyListList111, o2.PropertyListList111);
            }
        }
    }

    [Serializable]
    internal class ListRandomizableStructClass {
        private List<RandomizableStruct> _prFieldListList10;
        private List<RandomizableStruct> _prFieldListList11;
        private List<RandomizableStruct> _prFieldListList12;
        private List<RandomizableStruct> _prFieldListList13;
        private List<RandomizableStruct> _prFieldListList14;

        public List<RandomizableStruct> PrFieldListList10 {
            get { return _prFieldListList10; }
        }

        public List<RandomizableStruct> PrFieldListList11 {
            get { return _prFieldListList11; }
        }

        public List<RandomizableStruct> PrFieldListList12 {
            get { return _prFieldListList12; }
        }

        public List<RandomizableStruct> PrFieldListList13 {
            get { return _prFieldListList13; }
        }

        public List<RandomizableStruct> PrFieldListList14 {
            get { return _prFieldListList14; }
        }

        public List<RandomizableStruct> _pubFieldListList10;
        public List<RandomizableStruct> _pubFieldListList11;
        public List<RandomizableStruct> _pubFieldListList12;
        public List<RandomizableStruct> _pubFieldListList13;
        public List<RandomizableStruct> _pubFieldListList14;
        public List<RandomizableStruct> PropertyListList10 { get; set; }
        public List<RandomizableStruct> PropertyListList11 { get; set; }
        public List<RandomizableStruct> PropertyListList12 { get; set; }
        public List<RandomizableStruct> PropertyListList13 { get; set; }
        public List<RandomizableStruct> PropertyListList14 { get; set; }
        public List<RandomizableStruct> PropertyListList15 { get; set; }
        public List<RandomizableStruct> PropertyListList16 { get; set; }
        public List<RandomizableStruct> PropertyListList17 { get; set; }
        public List<RandomizableStruct> PropertyListList18 { get; set; }
        public List<RandomizableStruct> PropertyListList19 { get; set; }
        public List<RandomizableStruct> PropertyListList110 { get; set; }
        public List<RandomizableStruct> PropertyListList111 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldListList10 = Utils.Default<List<RandomizableStruct>>();
            _prFieldListList11 = Utils.RandomList<RandomizableStruct>(r);
            _prFieldListList12 = Utils.Default<List<RandomizableStruct>>();
            _prFieldListList13 = Utils.EmptyList<RandomizableStruct>();
            _prFieldListList14 = Utils.Default<List<RandomizableStruct>>();
            _pubFieldListList10 = Utils.Default<List<RandomizableStruct>>();
            _pubFieldListList11 = Utils.RandomList<RandomizableStruct>(r);
            _pubFieldListList12 = Utils.Default<List<RandomizableStruct>>();
            _pubFieldListList13 = Utils.EmptyList<RandomizableStruct>();
            _pubFieldListList14 = Utils.Default<List<RandomizableStruct>>();
            PropertyListList10 = Utils.Default<List<RandomizableStruct>>();
            PropertyListList11 = Utils.RandomList<RandomizableStruct>(r);
            PropertyListList12 = Utils.Default<List<RandomizableStruct>>();
            PropertyListList13 = Utils.EmptyList<RandomizableStruct>();
            PropertyListList14 = Utils.Default<List<RandomizableStruct>>();
            PropertyListList15 = Utils.RandomList<RandomizableStruct>(r);
            PropertyListList16 = Utils.Default<List<RandomizableStruct>>();
            PropertyListList17 = Utils.RandomList<RandomizableStruct>(r);
            PropertyListList18 = Utils.Default<List<RandomizableStruct>>();
            PropertyListList19 = Utils.EmptyList<RandomizableStruct>();
            PropertyListList110 = Utils.Default<List<RandomizableStruct>>();
            PropertyListList111 = Utils.RandomList<RandomizableStruct>(r);
        }
    }
}