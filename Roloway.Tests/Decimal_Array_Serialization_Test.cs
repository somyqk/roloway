﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Decimal_Array_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new DecimalArrayClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (DecimalArrayClass) s2.Deserialize(typeof (DecimalArrayClass));

                CollectionAssert.AreEqual(o1.PrFieldDecimalArr0, o2.PrFieldDecimalArr0);
                CollectionAssert.AreEqual(o1.PrFieldDecimalArr1, o2.PrFieldDecimalArr1);
                CollectionAssert.AreEqual(o1.PrFieldDecimalArr2, o2.PrFieldDecimalArr2);
                CollectionAssert.AreEqual(o1.PrFieldDecimalArr3, o2.PrFieldDecimalArr3);
                CollectionAssert.AreEqual(o1.PrFieldDecimalArr4, o2.PrFieldDecimalArr4);
                CollectionAssert.AreEqual(o1._pubFieldDecimalArr0, o2._pubFieldDecimalArr0);
                CollectionAssert.AreEqual(o1._pubFieldDecimalArr1, o2._pubFieldDecimalArr1);
                CollectionAssert.AreEqual(o1._pubFieldDecimalArr2, o2._pubFieldDecimalArr2);
                CollectionAssert.AreEqual(o1._pubFieldDecimalArr3, o2._pubFieldDecimalArr3);
                CollectionAssert.AreEqual(o1._pubFieldDecimalArr4, o2._pubFieldDecimalArr4);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr0, o2.PropertyDecimalArr0);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr1, o2.PropertyDecimalArr1);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr2, o2.PropertyDecimalArr2);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr3, o2.PropertyDecimalArr3);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr4, o2.PropertyDecimalArr4);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr5, o2.PropertyDecimalArr5);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr6, o2.PropertyDecimalArr6);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr7, o2.PropertyDecimalArr7);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr8, o2.PropertyDecimalArr8);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr9, o2.PropertyDecimalArr9);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr10, o2.PropertyDecimalArr10);
                CollectionAssert.AreEqual(o1.PropertyDecimalArr11, o2.PropertyDecimalArr11);
            }
        }
    }

    [Serializable]
    internal class DecimalArrayClass {
        private Decimal[] _prFieldDecimalArr0;
        private Decimal[] _prFieldDecimalArr1;
        private Decimal[] _prFieldDecimalArr2;
        private Decimal[] _prFieldDecimalArr3;
        private Decimal[] _prFieldDecimalArr4;

        public Decimal[] PrFieldDecimalArr0 {
            get { return _prFieldDecimalArr0; }
        }

        public Decimal[] PrFieldDecimalArr1 {
            get { return _prFieldDecimalArr1; }
        }

        public Decimal[] PrFieldDecimalArr2 {
            get { return _prFieldDecimalArr2; }
        }

        public Decimal[] PrFieldDecimalArr3 {
            get { return _prFieldDecimalArr3; }
        }

        public Decimal[] PrFieldDecimalArr4 {
            get { return _prFieldDecimalArr4; }
        }

        public Decimal[] _pubFieldDecimalArr0;
        public Decimal[] _pubFieldDecimalArr1;
        public Decimal[] _pubFieldDecimalArr2;
        public Decimal[] _pubFieldDecimalArr3;
        public Decimal[] _pubFieldDecimalArr4;
        public Decimal[] PropertyDecimalArr0 { get; set; }
        public Decimal[] PropertyDecimalArr1 { get; set; }
        public Decimal[] PropertyDecimalArr2 { get; set; }
        public Decimal[] PropertyDecimalArr3 { get; set; }
        public Decimal[] PropertyDecimalArr4 { get; set; }
        public Decimal[] PropertyDecimalArr5 { get; set; }
        public Decimal[] PropertyDecimalArr6 { get; set; }
        public Decimal[] PropertyDecimalArr7 { get; set; }
        public Decimal[] PropertyDecimalArr8 { get; set; }
        public Decimal[] PropertyDecimalArr9 { get; set; }
        public Decimal[] PropertyDecimalArr10 { get; set; }
        public Decimal[] PropertyDecimalArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldDecimalArr0 = Utils.Default<Decimal[]>();
            _prFieldDecimalArr1 = Utils.RandomArray<Decimal>(r);
            _prFieldDecimalArr2 = Utils.Default<Decimal[]>();
            _prFieldDecimalArr3 = Utils.EmptyArray<Decimal>();
            _prFieldDecimalArr4 = Utils.Default<Decimal[]>();
            _pubFieldDecimalArr0 = Utils.Default<Decimal[]>();
            _pubFieldDecimalArr1 = Utils.RandomArray<Decimal>(r);
            _pubFieldDecimalArr2 = Utils.Default<Decimal[]>();
            _pubFieldDecimalArr3 = Utils.EmptyArray<Decimal>();
            _pubFieldDecimalArr4 = Utils.Default<Decimal[]>();
            PropertyDecimalArr0 = Utils.Default<Decimal[]>();
            PropertyDecimalArr1 = Utils.RandomArray<Decimal>(r);
            PropertyDecimalArr2 = Utils.Default<Decimal[]>();
            PropertyDecimalArr3 = Utils.EmptyArray<Decimal>();
            PropertyDecimalArr4 = Utils.Default<Decimal[]>();
            PropertyDecimalArr5 = Utils.RandomArray<Decimal>(r);
            PropertyDecimalArr6 = Utils.Default<Decimal[]>();
            PropertyDecimalArr7 = Utils.RandomArray<Decimal>(r);
            PropertyDecimalArr8 = Utils.Default<Decimal[]>();
            PropertyDecimalArr9 = Utils.EmptyArray<Decimal>();
            PropertyDecimalArr10 = Utils.Default<Decimal[]>();
            PropertyDecimalArr11 = Utils.RandomArray<Decimal>(r);
        }
    }
}