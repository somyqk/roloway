﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Array_of_Enum_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ArrrayEnumClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ArrrayEnumClass) s2.Deserialize(typeof (ArrrayEnumClass));

                CollectionAssert.AreEqual(o1.PrFieldSomeEnumArr0, o2.PrFieldSomeEnumArr0);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumArr1, o2.PrFieldSomeEnumArr1);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumArr2, o2.PrFieldSomeEnumArr2);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumArr3, o2.PrFieldSomeEnumArr3);
                CollectionAssert.AreEqual(o1.PrFieldSomeEnumArr4, o2.PrFieldSomeEnumArr4);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumArr0, o2._pubFieldSomeEnumArr0);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumArr1, o2._pubFieldSomeEnumArr1);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumArr2, o2._pubFieldSomeEnumArr2);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumArr3, o2._pubFieldSomeEnumArr3);
                CollectionAssert.AreEqual(o1._pubFieldSomeEnumArr4, o2._pubFieldSomeEnumArr4);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr0, o2.PropertySomeEnumArr0);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr1, o2.PropertySomeEnumArr1);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr2, o2.PropertySomeEnumArr2);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr3, o2.PropertySomeEnumArr3);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr4, o2.PropertySomeEnumArr4);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr5, o2.PropertySomeEnumArr5);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr6, o2.PropertySomeEnumArr6);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr7, o2.PropertySomeEnumArr7);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr8, o2.PropertySomeEnumArr8);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr9, o2.PropertySomeEnumArr9);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr10, o2.PropertySomeEnumArr10);
                CollectionAssert.AreEqual(o1.PropertySomeEnumArr11, o2.PropertySomeEnumArr11);
            }
        }
    }

    [Serializable]
    internal class ArrrayEnumClass {
        private SomeEnum[] _prFieldSomeEnumArr0;
        private SomeEnum[] _prFieldSomeEnumArr1;
        private SomeEnum[] _prFieldSomeEnumArr2;
        private SomeEnum[] _prFieldSomeEnumArr3;
        private SomeEnum[] _prFieldSomeEnumArr4;

        public SomeEnum[] PrFieldSomeEnumArr0 {
            get { return _prFieldSomeEnumArr0; }
        }

        public SomeEnum[] PrFieldSomeEnumArr1 {
            get { return _prFieldSomeEnumArr1; }
        }

        public SomeEnum[] PrFieldSomeEnumArr2 {
            get { return _prFieldSomeEnumArr2; }
        }

        public SomeEnum[] PrFieldSomeEnumArr3 {
            get { return _prFieldSomeEnumArr3; }
        }

        public SomeEnum[] PrFieldSomeEnumArr4 {
            get { return _prFieldSomeEnumArr4; }
        }

        public SomeEnum[] _pubFieldSomeEnumArr0;
        public SomeEnum[] _pubFieldSomeEnumArr1;
        public SomeEnum[] _pubFieldSomeEnumArr2;
        public SomeEnum[] _pubFieldSomeEnumArr3;
        public SomeEnum[] _pubFieldSomeEnumArr4;
        public SomeEnum[] PropertySomeEnumArr0 { get; set; }
        public SomeEnum[] PropertySomeEnumArr1 { get; set; }
        public SomeEnum[] PropertySomeEnumArr2 { get; set; }
        public SomeEnum[] PropertySomeEnumArr3 { get; set; }
        public SomeEnum[] PropertySomeEnumArr4 { get; set; }
        public SomeEnum[] PropertySomeEnumArr5 { get; set; }
        public SomeEnum[] PropertySomeEnumArr6 { get; set; }
        public SomeEnum[] PropertySomeEnumArr7 { get; set; }
        public SomeEnum[] PropertySomeEnumArr8 { get; set; }
        public SomeEnum[] PropertySomeEnumArr9 { get; set; }
        public SomeEnum[] PropertySomeEnumArr10 { get; set; }
        public SomeEnum[] PropertySomeEnumArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldSomeEnumArr0 = Utils.Default<SomeEnum[]>();
            _prFieldSomeEnumArr1 = Utils.RandomArray<SomeEnum>(r);
            _prFieldSomeEnumArr2 = Utils.Default<SomeEnum[]>();
            _prFieldSomeEnumArr3 = Utils.EmptyArray<SomeEnum>();
            _prFieldSomeEnumArr4 = Utils.Default<SomeEnum[]>();
            _pubFieldSomeEnumArr0 = Utils.Default<SomeEnum[]>();
            _pubFieldSomeEnumArr1 = Utils.RandomArray<SomeEnum>(r);
            _pubFieldSomeEnumArr2 = Utils.Default<SomeEnum[]>();
            _pubFieldSomeEnumArr3 = Utils.EmptyArray<SomeEnum>();
            _pubFieldSomeEnumArr4 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr0 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr1 = Utils.RandomArray<SomeEnum>(r);
            PropertySomeEnumArr2 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr3 = Utils.EmptyArray<SomeEnum>();
            PropertySomeEnumArr4 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr5 = Utils.RandomArray<SomeEnum>(r);
            PropertySomeEnumArr6 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr7 = Utils.RandomArray<SomeEnum>(r);
            PropertySomeEnumArr8 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr9 = Utils.EmptyArray<SomeEnum>();
            PropertySomeEnumArr10 = Utils.Default<SomeEnum[]>();
            PropertySomeEnumArr11 = Utils.RandomArray<SomeEnum>(r);
        }
    }
}