﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Array_of_Class_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new ArrayClassClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (ArrayClassClass) s2.Deserialize(typeof (ArrayClassClass));

                CollectionAssert.AreEqual(o1.PrFieldRandomizableArr0, o2.PrFieldRandomizableArr0);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableArr1, o2.PrFieldRandomizableArr1);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableArr2, o2.PrFieldRandomizableArr2);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableArr3, o2.PrFieldRandomizableArr3);
                CollectionAssert.AreEqual(o1.PrFieldRandomizableArr4, o2.PrFieldRandomizableArr4);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableArr0, o2._pubFieldRandomizableArr0);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableArr1, o2._pubFieldRandomizableArr1);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableArr2, o2._pubFieldRandomizableArr2);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableArr3, o2._pubFieldRandomizableArr3);
                CollectionAssert.AreEqual(o1._pubFieldRandomizableArr4, o2._pubFieldRandomizableArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr0, o2.PropertyRandomizableArr0);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr1, o2.PropertyRandomizableArr1);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr2, o2.PropertyRandomizableArr2);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr3, o2.PropertyRandomizableArr3);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr4, o2.PropertyRandomizableArr4);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr5, o2.PropertyRandomizableArr5);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr6, o2.PropertyRandomizableArr6);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr7, o2.PropertyRandomizableArr7);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr8, o2.PropertyRandomizableArr8);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr9, o2.PropertyRandomizableArr9);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr10, o2.PropertyRandomizableArr10);
                CollectionAssert.AreEqual(o1.PropertyRandomizableArr11, o2.PropertyRandomizableArr11);
            }
        }
    }

    [Serializable]
    internal class ArrayClassClass {
        private Randomizable[] _prFieldRandomizableArr0;
        private Randomizable[] _prFieldRandomizableArr1;
        private Randomizable[] _prFieldRandomizableArr2;
        private Randomizable[] _prFieldRandomizableArr3;
        private Randomizable[] _prFieldRandomizableArr4;

        public Randomizable[] PrFieldRandomizableArr0 {
            get { return _prFieldRandomizableArr0; }
        }

        public Randomizable[] PrFieldRandomizableArr1 {
            get { return _prFieldRandomizableArr1; }
        }

        public Randomizable[] PrFieldRandomizableArr2 {
            get { return _prFieldRandomizableArr2; }
        }

        public Randomizable[] PrFieldRandomizableArr3 {
            get { return _prFieldRandomizableArr3; }
        }

        public Randomizable[] PrFieldRandomizableArr4 {
            get { return _prFieldRandomizableArr4; }
        }

        public Randomizable[] _pubFieldRandomizableArr0;
        public Randomizable[] _pubFieldRandomizableArr1;
        public Randomizable[] _pubFieldRandomizableArr2;
        public Randomizable[] _pubFieldRandomizableArr3;
        public Randomizable[] _pubFieldRandomizableArr4;
        public Randomizable[] PropertyRandomizableArr0 { get; set; }
        public Randomizable[] PropertyRandomizableArr1 { get; set; }
        public Randomizable[] PropertyRandomizableArr2 { get; set; }
        public Randomizable[] PropertyRandomizableArr3 { get; set; }
        public Randomizable[] PropertyRandomizableArr4 { get; set; }
        public Randomizable[] PropertyRandomizableArr5 { get; set; }
        public Randomizable[] PropertyRandomizableArr6 { get; set; }
        public Randomizable[] PropertyRandomizableArr7 { get; set; }
        public Randomizable[] PropertyRandomizableArr8 { get; set; }
        public Randomizable[] PropertyRandomizableArr9 { get; set; }
        public Randomizable[] PropertyRandomizableArr10 { get; set; }
        public Randomizable[] PropertyRandomizableArr11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldRandomizableArr0 = Utils.Default<Randomizable[]>();
            _prFieldRandomizableArr1 = Utils.RandomArray<Randomizable>(r);
            _prFieldRandomizableArr2 = Utils.Default<Randomizable[]>();
            _prFieldRandomizableArr3 = Utils.EmptyArray<Randomizable>();
            _prFieldRandomizableArr4 = Utils.Default<Randomizable[]>();
            _pubFieldRandomizableArr0 = Utils.Default<Randomizable[]>();
            _pubFieldRandomizableArr1 = Utils.RandomArray<Randomizable>(r);
            _pubFieldRandomizableArr2 = Utils.Default<Randomizable[]>();
            _pubFieldRandomizableArr3 = Utils.EmptyArray<Randomizable>();
            _pubFieldRandomizableArr4 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr0 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr1 = Utils.RandomArray<Randomizable>(r);
            PropertyRandomizableArr2 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr3 = Utils.EmptyArray<Randomizable>();
            PropertyRandomizableArr4 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr5 = Utils.RandomArray<Randomizable>(r);
            PropertyRandomizableArr6 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr7 = Utils.RandomArray<Randomizable>(r);
            PropertyRandomizableArr8 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr9 = Utils.EmptyArray<Randomizable>();
            PropertyRandomizableArr10 = Utils.Default<Randomizable[]>();
            PropertyRandomizableArr11 = Utils.RandomArray<Randomizable>(r);
        }
    }
}