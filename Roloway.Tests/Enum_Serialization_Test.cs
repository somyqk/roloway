﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Roloway.Lib.Impl;
using Roloway.Tests.Others;

namespace Roloway.Tests {
    [TestClass]
    public class Enum_Serialization_Test {
        [TestMethod]
        public void Can_Correctly_Deserialize() {
            using (var stream = new MemoryStream()) {
                var o1 = new EnumClass();
                o1.Init();

                var s1 = new RolowaySerializer(stream);
                s1.Serialize(o1);

                stream.Position = 0;
                var s2 = new RolowaySerializer(stream);
                var o2 = (EnumClass) s2.Deserialize(typeof (EnumClass));

                Assert.AreEqual(o1.PrFieldSomeEnum0, o2.PrFieldSomeEnum0);
                Assert.AreEqual(o1.PrFieldSomeEnum1, o2.PrFieldSomeEnum1);
                Assert.AreEqual(o1.PrFieldSomeEnum2, o2.PrFieldSomeEnum2);
                Assert.AreEqual(o1.PrFieldSomeEnum3, o2.PrFieldSomeEnum3);
                Assert.AreEqual(o1.PrFieldSomeEnum4, o2.PrFieldSomeEnum4);
                Assert.AreEqual(o1._pubFieldSomeEnum0, o2._pubFieldSomeEnum0);
                Assert.AreEqual(o1._pubFieldSomeEnum1, o2._pubFieldSomeEnum1);
                Assert.AreEqual(o1._pubFieldSomeEnum2, o2._pubFieldSomeEnum2);
                Assert.AreEqual(o1._pubFieldSomeEnum3, o2._pubFieldSomeEnum3);
                Assert.AreEqual(o1._pubFieldSomeEnum4, o2._pubFieldSomeEnum4);
                Assert.AreEqual(o1.PropertySomeEnum0, o2.PropertySomeEnum0);
                Assert.AreEqual(o1.PropertySomeEnum1, o2.PropertySomeEnum1);
                Assert.AreEqual(o1.PropertySomeEnum2, o2.PropertySomeEnum2);
                Assert.AreEqual(o1.PropertySomeEnum3, o2.PropertySomeEnum3);
                Assert.AreEqual(o1.PropertySomeEnum4, o2.PropertySomeEnum4);
                Assert.AreEqual(o1.PropertySomeEnum5, o2.PropertySomeEnum5);
                Assert.AreEqual(o1.PropertySomeEnum6, o2.PropertySomeEnum6);
                Assert.AreEqual(o1.PropertySomeEnum7, o2.PropertySomeEnum7);
                Assert.AreEqual(o1.PropertySomeEnum8, o2.PropertySomeEnum8);
                Assert.AreEqual(o1.PropertySomeEnum9, o2.PropertySomeEnum9);
                Assert.AreEqual(o1.PropertySomeEnum10, o2.PropertySomeEnum10);
                Assert.AreEqual(o1.PropertySomeEnum11, o2.PropertySomeEnum11);
            }
        }
    }

    [Serializable]
    internal class EnumClass {
        private SomeEnum _prFieldSomeEnum0;
        private SomeEnum _prFieldSomeEnum1;
        private SomeEnum _prFieldSomeEnum2;
        private SomeEnum _prFieldSomeEnum3;
        private SomeEnum _prFieldSomeEnum4;

        public SomeEnum PrFieldSomeEnum0 {
            get { return _prFieldSomeEnum0; }
        }

        public SomeEnum PrFieldSomeEnum1 {
            get { return _prFieldSomeEnum1; }
        }

        public SomeEnum PrFieldSomeEnum2 {
            get { return _prFieldSomeEnum2; }
        }

        public SomeEnum PrFieldSomeEnum3 {
            get { return _prFieldSomeEnum3; }
        }

        public SomeEnum PrFieldSomeEnum4 {
            get { return _prFieldSomeEnum4; }
        }

        public SomeEnum _pubFieldSomeEnum0;
        public SomeEnum _pubFieldSomeEnum1;
        public SomeEnum _pubFieldSomeEnum2;
        public SomeEnum _pubFieldSomeEnum3;
        public SomeEnum _pubFieldSomeEnum4;
        public SomeEnum PropertySomeEnum0 { get; set; }
        public SomeEnum PropertySomeEnum1 { get; set; }
        public SomeEnum PropertySomeEnum2 { get; set; }
        public SomeEnum PropertySomeEnum3 { get; set; }
        public SomeEnum PropertySomeEnum4 { get; set; }
        public SomeEnum PropertySomeEnum5 { get; set; }
        public SomeEnum PropertySomeEnum6 { get; set; }
        public SomeEnum PropertySomeEnum7 { get; set; }
        public SomeEnum PropertySomeEnum8 { get; set; }
        public SomeEnum PropertySomeEnum9 { get; set; }
        public SomeEnum PropertySomeEnum10 { get; set; }
        public SomeEnum PropertySomeEnum11 { get; set; }

        public void Init() {
            var r = new Random();
            _prFieldSomeEnum0 = Utils.Default<SomeEnum>();
            _prFieldSomeEnum1 = Utils.RandomValue<SomeEnum>(r);
            _prFieldSomeEnum2 = Utils.RandomValue<SomeEnum>(r);
            _prFieldSomeEnum3 = Utils.Default<SomeEnum>();
            _prFieldSomeEnum4 = Utils.RandomValue<SomeEnum>(r);
            _pubFieldSomeEnum0 = Utils.Default<SomeEnum>();
            _pubFieldSomeEnum1 = Utils.RandomValue<SomeEnum>(r);
            _pubFieldSomeEnum2 = Utils.Default<SomeEnum>();
            _pubFieldSomeEnum3 = Utils.RandomValue<SomeEnum>(r);
            _pubFieldSomeEnum4 = Utils.Default<SomeEnum>();
            PropertySomeEnum0 = Utils.Default<SomeEnum>();
            PropertySomeEnum1 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum2 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum3 = Utils.Default<SomeEnum>();
            PropertySomeEnum4 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum5 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum6 = Utils.Default<SomeEnum>();
            PropertySomeEnum7 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum8 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum9 = Utils.Default<SomeEnum>();
            PropertySomeEnum10 = Utils.RandomValue<SomeEnum>(r);
            PropertySomeEnum11 = Utils.RandomValue<SomeEnum>(r);
        }
    }
}