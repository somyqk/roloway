Roloway - двоичный сериализатор (simple binary serializer)
=========

Roloway является сериализатором объектов .NET в нехитром, но компактном двоичном формате.

Пример сериализации объекта: 

```csharp
using (var stream = new MemoryStream()) {
    var person = new Person(){
        FirstName = "Samat",
        LastName = "Ait"
    };
    var serializer = new RolowaySerializer(stream);
    serializer.Serialize(person);
}
```

Пример десериализции объекта:
```csharp
var serializer = new RolowaySerializer(stream);
Person p = (Person) serializer.Deserialize(typeof (Person));
```

Как видно из примеров выше, использование сериализатора просто и интуитивно. `RolowaySerializer` сериализует экземпляры классов помеченные атрибутом `[Serializable]`, иначе будет выброшено исключение типа `SerializationException`.
```csharp
[Serializable]
class Person { ... }
```

ВНИМАНИЕ!
----
Данный сериализатор, сериализует все **публичные (public)**, **не публичные (private, protected, internal)** и **скрытые (backing fields)** поля (**fields**) класса.

Как устроено решение? (solution contents)
----
Решение состоит из 3 проектов:

+ `Rolaway.Console` - консольный проект, для демонстрации использования сериализатора. Полный пример использования сериализатора показан в исходниках `Program.cs`. Данный проект является запускающимся по умолчанию (StartUp Project).
+ `Roloway.Lib` - библиотечный проект, исходные коды сериализатора и используемых компонентов находятся здесь.
+ `Roloway.Tests` - проект для тестов. В данном проекте находятся больше 50-ти тестов с разными сценариями для проверки работоспособности сериализатора. Исходники тестов также могут служить в качестве примеров использования сериализатора.

Как все работает? (how it works?)
----

Перед тем как приступить к описанию работы сериализатора и используемого двоичного формата, полезно узнать основные типы библиотеки:

+ `Roloway.Lib.Abstract.ISbSerializer` - интерфейс который определяет два метода:
    * `void Serialize(Stream stream, object obj, Tables tables);`
    * `object Deserialize(Stream stream, Type type, Tables tables, object result);`

    Все внутренние компоненты сериализатора реализуют данный интерфейс. Однако сам `RolowaySerializer` не реализует этот интерфейс, а является лишь фасадом для внутренних компонент (facade pattern).

+ Классы реализующие интерфейс `ISbSerializer` находятся в области (namespace) `Roloway.Lib.Impl.Serializers`. Объекты этих классов являются внутренними компонентами которые используются в `RolowaySerializer`. Каждый компонент, реализующий интерфейс `ISbSerializer`, может сериализовать и десериализовать определенный фундаментальный тип. К примеру, есть компонент `Int64Serializer`. Задача этого компонента сериализация и десериализация типов Int64.

+ `Roloway.Lib.Impl.Serializers.Factory` - фабрика компонентов `ISbSerializer` (factory method pattern). Фабрика является синглтоном (pattern singleton), и возвращает определенную реализацию интерфейса `ISbSerializer` в зависимости от сериализуемого типа. Таким образом, мы проводим линию абстракции между конкретной реализации и вызывающей стороной.

Для того чтобы понять как работает сериализатор, рассмотрим следующий пример:
```csharp
using (var stream = new MemoryStream()) {
    var person = new Person(){
        FirstName = "Samat",
        LastName = "Ait"
    };
    var serializer = new RolowaySerializer(stream);
    serializer.Serialize(person);
}
```
При вызове метода `serializer.Serialize(person);`, вызывается цепочка компонентов `ISbSerializer` (pattern chain of responsibility) в следующем порядке:

+ `HeaderSerializer -> FieldTypesSerializer -> FieldNamesSerializer -> FieldValuesSerializer`

Все эти классы находятся в области `Roloway.Lib.Impl.Serializers.Meta`, так как эти компоненты не занимаются сериализацией данных. Все вышеперечисленные компоненты, кроме `FieldValuesSerializer`, выполняют сериализацию служебной информации для облегчения выполнения десериализации в последующем.

+ `HeaderSerializer` - компонент который записывает в поток заголовок формата (4 байта CSE!), и полное наименование сериализуемого типа. При десериализации, если заголовок потока неверный, либо тип не совпадает, будет выброшено исключение.

+ `FieldTypesSerializer` - компонент который записывает в поток типы полей (публичных, не публичных и скрытых) сериализуемого объекта.

+ `FieldNamesSerializer` - компонент который записывает в поток наименования полей.

+ `FieldValuesSerializer` - компонент который записывает в поток значения полей. Данные объекта сериализуются этим компонентом. Однако, стоит упомянуть, что этот компонент сам по себе понятия не имеет как сериализовать данные. Для этого он обращается к фабрике компонентов `Factory`, который достает конкретный сериализатор в зависимости от типа, в интерфейсе `ISbSerializer`. К примеру:

    `class Person { private int _age; ... }`

    При сериализации объекта класса `Person`, компонент `FieldValuesSerializer` запросит у `Factory` сериализатор для типа `int`. Фабрика `Factory`, видя что требуется сериализатор для типа `Int32`, возвращает экземпляр класса `Roloway.Lib.Impl.Serializers.ValueType.Numeric.Int32Serializer`. Точно так же происходит и для всех остальных полей.

Немного о формате сериализации
----

Рассмотрим на примере `Roloway.Lib.Impl.Serializers.ValueType.Numeric.Int64Serializer` как сериализуются данные типа `Int64` т.е. `long`.

Так как тип `long` для представления использует 8 байтов, можно было бы не парясь загнать все 8 байтов в поток. Однако, если мы хотим записать в поток число 1, тогда двоичное представление числа типа `long` выглядело бы так:

`0000 0001` 1-ый байт

`0000 0000` 2-ой байт

`0000 0000` 3-ий байт

`0000 0000` 4-ый байт

`0000 0000` 5-ый байт

`0000 0000` 6-ой байт

`0000 0000` 7-ой байт

`0000 0000` 8-ой байт

Как видим, все байты кроме первого являются пустыми. Поэтому, в целях оптимизации, применяется следующий алгоритм. При сериализации данных типа `long`, один байт записывается для обозначения длины ненулевых байтов, остальные байты записываются с первого ненулевого в обратном порядке.

К примеру, надо записать в поток число 7777 типа `long`. Двоичное представление числа 7777 следующее:

`0110 0001` 1-ый байт

`0001 1110` 2-ой байт

`0000 0000` 3-ий байт

`0000 0000` 4-ый байт

`0000 0000` 5-ый байт

`0000 0000` 6-ой байт

`0000 0000` 7-ой байт

`0000 0000` 8-ой байт

При сериализации, сначала запишется байт со значением `0000 0010` для того чтобы было понятно, что длина ненулевых байтов равняется 2-м. Далее байты записываются в обратном порядке, начиная с первого ненулевого байта. В итоге, в поток будут отправлены значения

`0000 0010` 1-ый байт

`0001 1110` 2-ой байт

`0110 0001` 3-ий байт

Для сериализации остальных численных типов, используется аналогичный алгоритм.

Поддерживаемые типы для сериализации
----

Справа от наименовании типов, обозначены используемые сериализаторы

`byte` <=> `Roloway.Lib.Impl.Serializers.ValueType.Numeric.ByteSerializer`

`sbyte` <=> `Roloway.Lib.Impl.Serializers.ValueType.Numeric.SByteSerializer`

`ushort` <=> `Roloway.Lib.Impl.Serializers.ValueType.Numeric.UInt16Serializer`

`short` <=> `Roloway.Lib.Impl.Serializers.ValueType.Numeric.Int16Serializer`

`uint` <=> `Roloway.Lib.Impl.Serializers.ValueType.Numeric.UInt32Serializer`

`int` <=> `Roloway.Lib.Impl.Serializers.ValueType.Numeric.Int32Serializer`

`ulong` <=> `Roloway.Lib.Impl.Serializers.ValueType.Numeric.UInt64Serializer`

`long` <=> `Roloway.Lib.Impl.Serializers.ValueType.Numeric.Int64Serializer`

`float` <=> `Roloway.Lib.Impl.Serializers.ValueType.Numeric.SingleSerializer`

`double` <=> `Roloway.Lib.Impl.Serializers.ValueType.Numeric.DoubleSerializer`

`bool` <=> `Roloway.Lib.Impl.Serializers.ValueType.BooleanSerializer`

`char` <=> `Roloway.Lib.Impl.Serializers.ValueType.CharSerializer`

`DateTime` <=> `Roloway.Lib.Impl.Serializers.ValueType.DateTimeSerializer`

`decimal` <=> `Roloway.Lib.Impl.Serializers.ValueType.DecimalSerializer`

`string` <=> `Roloway.Lib.Impl.Serializers.RefType.StringSerializer`

для сериализации иных `enum` <=> `Roloway.Lib.Impl.Serializers.ValueType.EnumSerializer`

для сериализации структур <=> `Roloway.Lib.Impl.Serializers.ValueType.StructSerializer`

для сериализации классов <=> `Roloway.Lib.Impl.Serializers.RefType.ClassSerializer`

для сериализации массивов <=> `Roloway.Lib.Impl.Serializers.RefType.ArraySerializer`


Почему сериализатор был назван Roloway?
----

Если вы дочитали до этого места, ну что же, поздравляю, так и быть, объясню почему сериализатор был назван Roloway. В ходе выполнения данного задания, по телевизору шла передача о редких, исчезающих видах животных. Одна из них, обезьяна вида Roloway. Вот оттуда и пошло название. Не стоит искать особого глубокого смысла в этом.